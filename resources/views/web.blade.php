<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <!-- Icons -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}"/>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('web/css/app.css') }}">
</head>
<body>
    <div id="app" class="mode-web"></div>
    <script src="{{ mix('web/js/app.js') }}"></script>
</body>
</html>
