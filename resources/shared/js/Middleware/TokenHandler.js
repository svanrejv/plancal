import axios from 'axios';
import useAuthStore from '../Stores/auth';

axios.interceptors.request.use(
    async (config) => {
        const authStore = useAuthStore();
        if (await authStore.isLoggedIn()) {
            const fbToken = await authStore.getAuthToken();
            if (fbToken) {
                config.headers.Authorization = `Bearer ${fbToken}`;
            }
        }

        return config;
    }
);

axios.interceptors.response.use(
    (response) => {
        return response;
    },

    (error) => {
        if (
            error.response &&
            error.response.status === 401 &&
            !error.config.url.includes('auth/login') &&
            !error.config.url.includes('auth/logout')
        ) {
            const authStore = useAuthStore();
            authStore.logOut();
        }

        return Promise.reject(error);
    }
);
