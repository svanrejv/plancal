import axios from 'axios';
import Qs from 'qs';

axios.interceptors.request.use(
    (config) => {

        config.paramsSerializer = function(params) {
            return Qs.stringify(params, {arrayFormat: 'brackets', encodeValuesOnly: true});
        };
        return config;

    },

    (error) => {
        return Promise.reject(error);
    }
);
