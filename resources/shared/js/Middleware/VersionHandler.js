import axios from 'axios';

axios.interceptors.request.use(
    (config) => {
        // Axios doesn't support passing custom params, see: https://github.com/axios/axios/issues/2295
        // So until a better solution comes around, we'll use headers to pass the version to this interceptor
        const apiVersion = config.headers.apiVersion || 1;
        config.baseURL = config.baseURL + '/v' + apiVersion;
        return config;
    }
);
