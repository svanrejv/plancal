import i18n from './i18n';

export default function(user) {
    return (user) ? (user.first_name) : i18n.get('users.removed_user');
}
