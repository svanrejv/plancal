import pickBy from 'lodash/pickBy';
import mapKeys from 'lodash/mapKeys';
import startsWith from 'lodash/startsWith';
import trimStart from 'lodash/trimStart';
import replace from 'lodash/replace';

export default function(value, prefix, ...args) {
    return mapKeys(pickBy(value, (errors, fieldKey) => {
        return startsWith(fieldKey, prefix);
    }), (errors, fieldKey) => {
        return trimStart(replace(fieldKey, prefix, ''), '.');
    });
}
