import { sortBy } from 'lodash';

export class CtpHelper {

    static getClosestShot(strokes) {
        if(Array.isArray(strokes) && strokes.length > 0) {
            const sortedStrokes = sortBy(strokes, (stroke) => stroke.stroke_result.distance_to_hole);
            // TODO: Implement imperial
            return sortedStrokes[0].stroke_result.distance_to_hole + ' ' + 'cm';
        }
        // TODO: What happens when no strokes where made?
        return 'n/a';
    }

    static getAverage(strokes) {
        if(Array.isArray(strokes) && strokes.length > 0) {
            // TODO: Implement imperial
            const average = strokes.reduce((total, next) => total + next.stroke_result.distance_to_hole, 0) / strokes.length;
            return Math.round(average)  + ' ' + 'cm';
        }
        // TODO: What happens when no strokes where made?
        return 'n/a';
    }

}
