export default function(displayName) {
    if (typeof displayName !== 'string' || displayName.length < 3 || displayName.length > 20) {
        return false;
    }

    if (!displayName.match(/^[a-zA-Z0-9_-]*$/g)) {
        return false;
    }

    return true;
}
