import useProtocolStore from '../Stores/protocol';

export default function(scoreData, gameMode) {
    const protocol = useProtocolStore();
    switch (protocol.getEnumKey('game_mode', gameMode)) {
        case 'STROKE_PLAY':
            return scoreData.strokes;
        case 'CLOSEST_TO_PIN':
        case 'LONGDRIVE':
            return scoreData.score + ' cm'; // TODO: Convert to imperial depending on user settings
        default:
            return scoreData.score;
    }
}
