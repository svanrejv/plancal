import * as Sentry from '@sentry/vue';

export function reportError(error) {
    console.error(error);

    if (process.env.MIX_GAME_SENTRY_DSN || process.env.MIX_WEB_SENTRY_DSN) {
        Sentry.captureException(error);
    }
}
