import Lang from 'lang.js';
import compileForLangJS from './compileForLangJS';
import translations from '../../../lang/translations.json';

const i18n = new Lang({
    messages: compileForLangJS(translations),
    locale: 'en-US',
    fallback: 'en-US'
});

export default i18n;

