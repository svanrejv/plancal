export function getCourseImage(course, size = 'image_md', useFallback = true) {
    if (
        course &&
        course.images &&
        course.images.length &&
        course.images[0].sizes &&
        course.images[0].sizes.hasOwnProperty(size)
    ) {
        return course.images[0].sizes[size];
    }

    return (useFallback) ? '/assets/images/temp3.png' : null;
}
