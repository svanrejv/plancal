import Echo from 'laravel-echo';
import axios from 'axios';

window.Pusher = require('pusher-js');
if (process.env.MIX_APP_DEBUG === 'true') {
    window.Pusher.logToConsole = true;
}

const socket = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER || 'eu',
    forceTLS: true,
    authorizer: (channel, options) => {
        return {
            authorize: async (socketId, callback) => {
                try {
                    const response = await axios.post('/broadcasting/auth', {
                        socket_id: socketId,
                        channel_name: channel.name
                    });
                    callback(false, response.data.data);
                } catch (error) {
                    callback(true, error);
                }
            }
        };
    }
});

export default socket;
