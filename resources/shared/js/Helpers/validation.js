import map from 'lodash/map';
import flatMap from 'lodash/flatMap';
import i18n from './i18n';

export function flattenValidationErrors(error) {
    if (
        error.response &&
        error.response.status === 422 &&
        error.response.data.error_info.validation_errors &&
        Object.keys(error.response.data.error_info.validation_errors).length
    ) {
        return flatMap(
            error.response.data.error_info.validation_errors,
            (err) => map(err, (msg) => msg.charAt(0).toUpperCase() + msg.slice(1))
        ).join('\n');
    }

    return (error.response && error.response.data && error.response.data.error) ? error.response.data.error : i18n.get('generic.error_message');
}
