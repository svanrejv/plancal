export class ClubHelper {
    static isMember(clubs, clubId) {
        return Array.isArray(clubs) && clubs.some(e => e.id === clubId);
    }
}
