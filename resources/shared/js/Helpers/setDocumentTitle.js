export default function(title = null) {
    document.title = ((title) ?  (title + ' - ') : '') + process.env.MIX_APP_NAME;
}
