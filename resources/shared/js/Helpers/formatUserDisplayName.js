import i18n from './i18n';

export default function(user) {
    if(user) {
        if(user.display_name) {
            return user.display_name;
        } else if(user.first_name) {
            return user.first_name;
        }
    } else {
        return i18n.get('users.removed_user');
    }
}
