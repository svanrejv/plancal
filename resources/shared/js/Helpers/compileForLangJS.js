import each from 'lodash/each';
import merge from 'lodash/merge';
import mapKeys from 'lodash/mapKeys';

/**
 * Compiles Laravel-style translations to Lang.js compliant structure
 *
 * @param {Object} translations
 *
 * @return {Object}
 */
export default function (translations) {
    let compiled = {};

    each(translations, (keys, lang) => {
        merge(compiled, mapKeys(keys, (values, key) => {
            return lang + '.' + key;
        }));
    });

    return compiled;
}
