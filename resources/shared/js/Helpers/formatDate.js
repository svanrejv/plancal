import moment from 'moment';
import useAuthStore from '../Stores/auth';

export function formatDate(value, format = 'YYYY-MM-DD HH:mm', tz = 'UTC') {
    if (!value) {
        return value;
    }

    if (tz === 'UTC') {
        return moment.utc(value).local().format(format);
    } else {
        return moment(value).format(format);
    }
}

export function shortDate(date, withTime = false)
{
    if (!date) {
        return null;
    }
    const auth = useAuthStore();

    let options = (withTime) ? {
        weekday: 'long', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'
    } : {
        weekday: 'long', month: 'long', day: 'numeric'
    };

    return new Intl.DateTimeFormat(auth.getLocale(),options).format(Date.parse(date));
}
