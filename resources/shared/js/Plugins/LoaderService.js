import { defineAsyncComponent } from 'vue';
import mitt from 'mitt';

export default {
    install(app) {
        app.component('loader', defineAsyncComponent(() => import('../Components/Loader')));

        app.config.globalProperties.$loading = {
            events: mitt(),

            show() {
                app.config.globalProperties.$loading.events.emit('loading', true);
            },

            hide() {
                app.config.globalProperties.$loading.events.emit('loading', false);
            }
        };
    }
};
