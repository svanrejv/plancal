import eventBus from '../Helpers/eventBus';

export default {
    install(app) {
        app.config.globalProperties.$event = eventBus;
    }
};
