import { defineAsyncComponent } from 'vue';
import mitt from 'mitt';

export default {
    install(app) {
        app.component('error-modal', defineAsyncComponent(() => import('../Components/Modals/ErrorModal')));
        app.component('success-modal', defineAsyncComponent(() => import('../Components/Modals/SuccessModal')));
        app.component('confirm-modal', defineAsyncComponent(() => import('../Components/Modals/ConfirmModal')));

        app.config.globalProperties.$modal = {
            events: mitt(),

            show(modal, ...args) {
                app.config.globalProperties.$modal.events.emit(modal + '-show', args);
            }
        };
    }
};
