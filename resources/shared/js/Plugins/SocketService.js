import socket from '../Helpers/socket';

export default {
    install(app) {
        app.config.globalProperties.$socket = socket;
    }
};
