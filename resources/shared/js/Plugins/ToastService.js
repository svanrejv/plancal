import { defineAsyncComponent } from 'vue';
import mitt from 'mitt';

export default {
    install(app) {
        const events = mitt();
        app.component('toast', defineAsyncComponent(() => import('../Components/Toast')));

        app.config.globalProperties.$toast = {
            events,

            show(title, message, type, duration) {
                events.emit('show', { title, message, type, duration });
            }
        };
    }
};
