import { defineStore, acceptHMRUpdate } from 'pinia';
import axios from 'axios';
import useAuthStore from './auth';

const useProtocolStore = defineStore({
    id: 'protocol',

    state: () => {
        return {
            loadedLocale: null,
            enumsCache: {},
        };
    },

    actions: {
        clearCache() {
            this.loadedLocale = null;
            this.enumsCache = {};
        },

        /**
         * Returns enums from cache or fetches new if cache is empty
         *
         * @param {boolean} fresh
         * @returns {Promise<{}>}
         */
        loadEnums(fresh = false) {
            const authStore = useAuthStore();

            if (
                !fresh &&
                Object.keys(this.enumsCache).length &&
                this.loadedLocale === authStore.locale
            ) {
                return Promise.resolve(this.enumsCache);
            }

            const locale = (authStore.locale) ? authStore.locale : (window.navigator.language || 'en-US');

            return axios.get('protocol/enums/' + locale).then((enums) => {
                this.enumsCache = enums.data.data;
                this.loadedLocale = locale;

                return this.enumsCache;
            });
        },

        /**
         * Get all enums for a specific type
         *
         * @param {string} type
         * @returns {null|*}
         */
        getEnums(type) {
            if (type in this.enumsCache) {
                return this.enumsCache[type].values;
            } else {
                return null;
            }
        },

        /**
         * Return a single enum value based on type and key
         *
         * @param {string} type
         * @param {string|number} key
         * @returns {null|*}
         */
        getEnum(type, key) {
            if (type in this.enumsCache && key in this.enumsCache[type].values) {
                return this.enumsCache[type].values[key];
            } else {
                return null;
            }
        },

        /**
         * Return a single enum key based on type and value
         *
         * @param {string} type
         * @param {number} value
         * @returns {null|*}
         */
        getEnumKey(type, value) {
            if (type in this.enumsCache && Object.values(this.enumsCache[type].values).includes(value)) {
                return Object.keys(this.enumsCache[type].values).find(key => this.enumsCache[type].values[key] === value);
            } else {
                return null;
            }
        },

        /**
         * Gets translations for all enums for a specific type
         *
         * @param {string} type
         * @returns {null|*}
         */
        getTranslatedEnums(type) {
            if (type in this.enumsCache) {
                return this.enumsCache[type].i18n;
            } else {
                return null;
            }
        },

        /**
         * Get a single translated enum
         *
         * @param {string} type
         * @param {string} needle
         * @param {string} needleType Optionally set what to look for ("value" or "key")
         * @returns {null|*}
         */
        getTranslatedEnum(
            type,
            needle,
            needleType = 'value'
        ) {
            if (
                needleType === 'value' &&
                type in this.enumsCache &&
                needle in this.enumsCache[type].i18n
            ) {
                return this.enumsCache[type].i18n[needle];
            } else if (
                needleType === 'key' &&
                type in this.enumsCache
            ) {
                const value = this.getEnum(type, needle);

                if (value && value in this.enumsCache[type].i18n) {
                    return this.enumsCache[type].i18n[value];
                }
            }

            return null;
        }
    }
});

if (import.meta.webpackHot) {
    import.meta.webpackHot.accept(acceptHMRUpdate(useProtocolStore, import.meta.webpackHot));
}

export default useProtocolStore;
