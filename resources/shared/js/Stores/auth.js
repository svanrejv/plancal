import { defineStore, acceptHMRUpdate } from 'pinia';
import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged, setPersistence, browserLocalPersistence, signInWithEmailAndPassword, signOut, sendPasswordResetEmail } from 'firebase/auth';
import { markRaw } from 'vue';
import moment from 'moment';
import some from 'lodash/some';
import every from 'lodash/every';
import axios from 'axios';
import i18n from '../Helpers/i18n';
import socket from '../Helpers/socket';
import useProtocolStore from './protocol';
import useNotificationStore from './notifications';
import eventBus from '../Helpers/eventBus';

const useAuthStore = defineStore({
    id: 'auth',

    state: () => {
        return {
            user: null,
            abilities: null,
            locale: null,
            isRefreshingToken: false,
            isConnectedToSocket: false
        };
    },

    actions: {
        async init() {
            if (!this.firebase) {
                this.firebase = markRaw(initializeApp({
                    apiKey: process.env.MIX_FIREBASE_API_KEY,
                    authDomain: process.env.MIX_FIREBASE_AUTH_DOMAIN,
                    projectId: process.env.MIX_FIREBASE_PROJECT_ID,
                    storageBucket: process.env.MIX_FIREBASE_STORAGE_BUCKET,
                    messagingSenderId: process.env.MIX_FIREBASE_MESSAGING_SENDER_ID,
                    appId: process.env.MIX_FIREBASE_APP_ID
                }));

                const auth = getAuth();

                await setPersistence(auth, browserLocalPersistence);

                await (new Promise(resolve => {
                    onAuthStateChanged(auth, (user) => {
                        resolve(user);
                    });
                }));
            }

            if (await this.isLoggedIn()) {
                const loggedInUser = await this.getUser();
                this.listenForUserChanges(loggedInUser);
                return loggedInUser;
            }

            return true;
        },

        listenForUserChanges(loggedInUser) {
            if (this.isConnectedToSocket === true) {
                return false;
            }

            this.isConnectedToSocket = true;
            socket.private('users.' + loggedInUser.id)
                .listen('.user.updated', (event) => {
                    if (!event.user || !this.user) {
                        return;
                    }

                    this.onUserUpdated(event.user);
                })
                .listen('.wallet.updated', (event) => {
                    if (!event.wallet || !this.user) {
                        return;
                    }

                    this.onWalletUpdated(event.wallet);
                });
            const notifications = useNotificationStore();
            notifications.register();
        },

        onWalletUpdated(wallet) {
            if (!this.user.wallets) {
                this.user.wallets = [];
            }

            if (this.user.wallets.some((existingWallet) => existingWallet.token_type === wallet.token_type)) {
                this.user.wallets = this.user.wallets.map((existingWallet) => {
                    return (existingWallet.token_type === wallet.token_type) ? wallet : existingWallet;
                });
            } else {
                this.user.wallets.push(wallet);
            }

            return wallet;
        },

        onUserUpdated(user) {
            this.setUser(user);
        },

        async getUser(force = false) {
            if ((force || !this.user) && await this.isLoggedIn()) {
                const initRes = await Promise.all([
                    axios.get('/users/me')
                        .then(response => response.data.data)
                        .catch(err => {
                            return null;
                        }),
                    this.getAbilities(force)
                ]);
                this.setUser(initRes[0]);
            }

            return this.user;
        },

        setUser(user) {
            this.user = user;
            this.setLocale(user.locale);
        },

        async getAbilities(force = false) {
            if ((force || !this.abilities) && await this.isLoggedIn()) {
                this.setAbilities(
                    await axios.get('/users/me/abilities')
                        .then(response => response.data.data)
                        .catch(err => {
                            return null;
                        })
                );
            }

            return this.abilities;
        },

        setAbilities(abilities) {
            this.abilities = abilities;
        },

        checkAbility(ability) {
            if (Array.isArray(ability)) {
                return this.userCan(ability[0], ability[1], ability[2]);
            } else {
                return this.userCan(ability);
            }
        },

        userCan(ability, entityId, entityType) {
            if (!this.abilities) {
                return false;
            }

            return some(this.abilities, (listedAbility) => {
                if (entityId && entityType) {
                    return listedAbility.name === ability && listedAbility.entity_id === entityId && listedAbility.entity_type === entityType;
                } else {
                    return listedAbility.name === ability && listedAbility.entity_id === null && listedAbility.entity_type === null;
                }
            });
        },

        /**
         * Check against any of the provided abilities. Returns true on first match
         *
         * @param {array} abilities Array of abilities, either send in ability as string or as array of [abilityName, entityId, entityType]
         * @returns {boolean}
         */
        userCanAny(abilities) {
            if (!this.abilities) {
                return false;
            }

            return some(abilities, (ability) => this.checkAbility(ability));
        },

        /**
         * Check against all of the provided abilities. Return true only if all abilities match
         *
         * @param {array} abilities Array of abilities, either send in ability as string or as array of [abilityName, entityId, entityType]
         * @returns {boolean}
         */
        userCanAll(abilities) {
            if (!this.abilities) {
                return false;
            }

            return every(abilities, (ability) => this.checkAbility(ability));
        },

        setLocale(locale) {
            this.locale = locale;
            moment.locale(locale);
            i18n.setLocale(locale);
            axios.defaults.headers.common['Accept-Language'] = locale;
        },

        getLocale() {
            return this.locale;
        },

        async logIn(email, password) {
            await this.init();
            const auth = getAuth();
            await signInWithEmailAndPassword(auth, email, password);
            socket.connect();
            return this.getUser();
        },

        logOut(sendEvent = true) {
            socket.disconnect();

            this.isConnectedToSocket = false;
            this.user = null;
            this.abilities = null;

            if (this.firebase) {
                const auth = getAuth();
                signOut(auth);
            }

            const protocol = useProtocolStore();
            protocol.clearCache();

            if (sendEvent) {
                eventBus.emit('logout');
            }

            return true;
        },

        async isLoggedIn() {
            if (!this.firebase) {
                await this.init();
            }

            return !!(this.firebase && getAuth().currentUser);
        },

        getAuthToken() {
            const auth = getAuth();
            if (!this.firebase || !auth.currentUser) {
                return null;
            }

            return auth.currentUser.getIdToken();
        },

        async sendPasswordResetEmail(email) {
            if (!this.firebase) {
                await this.init();
            }

            return sendPasswordResetEmail(getAuth(), email);
        },

        async connectGameIdentifier(uuid, softwareVersion) {
            try {
                return await axios.post('/users/me/game-identifiers', {
                    data: {
                        uuid,
                        software_version: softwareVersion || null
                    }
                }).then(res => res.data.data);
            } catch (error) {
                this.logOut();
                throw error;
            }
        }
    }
});

if (import.meta.webpackHot) {
    import.meta.webpackHot.accept(acceptHMRUpdate(useAuthStore, import.meta.webpackHot));
}

export default useAuthStore;
