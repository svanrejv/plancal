import { defineStore, acceptHMRUpdate } from 'pinia';
import socket from '../Helpers/socket';
import useAuthStore from './auth';
import axios from 'axios';

const useNotificationStore = defineStore({
    id: 'notifications',

    state: () => {
        return {
            unread: 0
        };
    },

    actions: {
        async register() {
            const auth = useAuthStore();
            if (!auth.user) {
                return;
            }

            socket.private('users.' + auth.user.id).notification(this.onNotificationReceived);

            this.loadUnreadCount();
        },

        async loadUnreadCount() {
            const response = await axios.get('/users/me/notifications', {
                params: {
                    read: false,
                    per_page: 1
                }
            }).then(res => {
                return res.data.data;
            }).catch(error => null);

            this.unread = (response && response.total) ? response.total : 0;
            return this.unread;
        },

        onNotificationReceived(notification) {
            this.unread++;
            return notification;
        },

        readNotification() {
            this.unread--;
        },

        readAllNotification() {
            axios.patch('/users/me/notifications/all/read').catch(() => null);
            this.unread = 0;
        },

        getNotificationType(notification) {
            const type = ('type' in notification) ? notification.type : notification.data.notification_type;
            return type.split('\\').pop();
        },
    }
});

if (import.meta.webpackHot) {
    import.meta.webpackHot.accept(acceptHMRUpdate(useNotificationStore, import.meta.webpackHot));
}

export default useNotificationStore;
