import useAuthStore from './Stores/auth';

export function getCurrentUserSettings() {
    const auth = useAuthStore();
    return {
        'user': {
            'hand': auth.user.default_player_settings.hand,
            'seven_iron_distance': auth.user.default_player_settings['7_iron_distance'],
            'teeing_type': auth.user.default_player_settings.teeing_type
        },
        'hardware': {
            'os3': {
                'ballType': auth.user.default_player_settings.ball_type
            }
        }
    };
}
