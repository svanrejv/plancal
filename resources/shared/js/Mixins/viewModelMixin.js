export const viewModelMixin = {
    props: {
        id: String | Number,
        endpoint: String,
        eagerLoads: Array,
        eagerAppends: Array,
        pageTitle: String,
        modelNameSingular: String,
        modelTitleField: {
            type: String,
            default: function() {
                return 'name';
            }
        }
    },

    beforeMount() {
        this.initialize();
    },

    data() {
        return {
            isInitializing: true,
            definition: null,
            invalid: true,
            model: null,
            errors: {}
        };
    },

    methods: {
        async initialize() {
            this.$loading.show();
            this.isInitializing = true;
            try {
                await this.load(this.id);
                this.isInitializing = false;
            } catch (error) {
                this.$modal.show('error');
            }
            this.$loading.hide();
        },

        load(id = null) {
            let params = {};
            if (this.eagerLoads) {
                params.with = this.eagerLoads;
            }
            if (this.eagerAppends) {
                params.appends = this.eagerAppends;
            }
            return this.$http.get(this.endpoint + '/' + id, {
                params: params
            }).then((res) => {
                const data = res.data.data;
                this.model = data;
                return data;
            });
        }
    }
};
