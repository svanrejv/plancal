export const goBackMixin = {
    props: {
        backRoute: String
    },

    methods: {
        goBack(routeName) {
            this.$router.push({
                name: routeName
            });
        }
    }
};
