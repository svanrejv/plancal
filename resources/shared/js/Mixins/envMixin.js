export const envMixin = {
    data() {
        return {
            env: {
                APP_NAME: process.env.MIX_APP_NAME,
                APP_URL: process.env.MIX_GAME_URL,
                WEB_URL: process.env.MIX_WEB_URL
            }
        };
    }
};
