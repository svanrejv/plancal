import { defineAsyncComponent } from 'vue';
import mergeWith from 'lodash/mergeWith';
import debounce from 'lodash/debounce';
import concat from 'lodash/concat';
import moment from 'moment';

export const modelListMixin = {
    inject: ['$http', '$i18n'],

    components: {
        InlineLoader: defineAsyncComponent(() => import('../Components/InlineLoader')),
        ChevronDownIcon: defineAsyncComponent(() => import('../Icons/ChevronDown')),
        ChevronUpIcon: defineAsyncComponent(() => import('../Icons/ChevronUp')),
        ChevronDoubleLeftIcon: defineAsyncComponent(() => import('../Icons/ChevronDoubleLeft')),
        ChevronDoubleRightIcon: defineAsyncComponent(() => import('../Icons/ChevronDoubleRight')),
        PageFirstIcon: defineAsyncComponent(() => import('../Icons/PageFirst')),
        PageLastIcon: defineAsyncComponent(() => import('../Icons/PageLast'))
    },

    props: {
        endpoint: String,
        columns: Object,
        customParams: Object,
        defaultOrderBy: Object,
        searchColumns: String,
        searchQuery: String,
        splitSearchString: {
            type: Boolean,
            default: function() {
                return false;
            }
        },
        appendPages: {
            type: Boolean,
            default: function() {
                return false;
            }
        },
        perPage: {
            type: Number,
            default: function() {
                return 15;
            }
        },
        multiSort: {
            type: Boolean,
            default: function() {
                return false;
            }
        }
    },

    emits: [
        'columnClick',
        'orderBy',
        'loaded'
    ],

    async mounted() {
        this.isLoadingInitial = true;
        await this.load(this.pagination.currentPage);
        this.isLoadingInitial = false;
    },

    data() {
        return {
            isLoadingInitial: true,
            isLoadingInline: false,
            rows: null,
            orderBy: {},
            pagination: {
                currentPage: 1,
                lastPage: 1,
                total: 0
            }
        };
    },

    computed: {
        pages: function() {
            const pages = [];
            let paginationStart = 1;
            let paginationEnd = 1;

            if (this.pagination.currentPage > 4) {
                paginationStart = this.pagination.currentPage - 3;
            }

            if (this.pagination.lastPage > 4) {
                paginationEnd = ((this.pagination.currentPage + 3) <= this.pagination.lastPage) ? this.pagination.currentPage + 3 : this.pagination.lastPage;
            } else {
                paginationEnd = this.pagination.lastPage;
            }

            while (paginationStart <= paginationEnd) {
                pages.push(paginationStart);
                paginationStart++;
            }

            return pages;
        }
    },

    watch: {
        searchQuery: debounce(async function() {
            this.$loading.show();
            await this.load();
            this.$loading.hide();
        }, 300)
    },

    methods: {
        formatColumn: function(value, column) {
            let formattedValue;

            switch(column.type){
                case 'RELATION': {
                    const displayField = (column.displayField) ? column.displayField : 'id';
                    if (Array.isArray(value)) {
                        formattedValue = value.map((row) => row[displayField]).join(', ');
                    } else {
                        formattedValue = (value && value.hasOwnProperty(displayField)) ? value[displayField] : '';
                    }
                    break;
                }
                case 'TIMESTAMP':
                    formattedValue = (value) ? moment.utc(value).local().format('YYYY-MM-DD HH:mm') : value;
                    break;
                case 'DATE':
                    formattedValue = (value) ? moment.utc(value).local().format('YYYY-MM-DD') : value;
                    break;
                case 'ENUM': {
                    const splitValues = (Array.isArray(value)) ? value : ((typeof value === 'string') ? value.split(';') : [value]);
                    formattedValue = splitValues.map(val => (column.extras.options[val]) ? column.extras.options[val] : val).join(', ');
                    break;
                }
                case 'BOOL':
                    formattedValue = (value) ? this.$i18n.get('generic.yes') : this.$i18n.get('generic.no');
                    break;
                default:
                    formattedValue = value;
            }

            return formattedValue;
        },

        order: debounce(async function(column, order) {
            this.$emit('orderBy', column, order, this.columns);

            if (
                typeof this.columns[column] !== 'string' &&
                this.columns[column].hasOwnProperty('sortable') &&
                !this.columns[column].sortable
            ) {
                return;
            }

            const currentOrder = this.orderBy[column];

            if (!this.multiSort) {
                this.orderBy = {};
            }

            this.orderBy[column] = (order) ? order : ((currentOrder === 'desc') ? 'asc' : 'desc');

            this.$loading.show();
            await this.load(this.pagination.currentPage);
            this.$loading.hide();
        }, 300),

        loadPage: debounce(async function(page, appendResult = false, isInitial = false) {
            if (page < 1) {
                page = 1;
            }

            if (page > this.pagination.lastPage) {
                page = this.pagination.lastPage;
            }

            if (isInitial) {
                this.isLoadingInitial = true;
            } else if (appendResult) {
                this.isLoadingInline = true;
            } else {
                this.$loading.show();
            }

            await this.load(page, appendResult);

            if (isInitial) {
                this.isLoadingInitial = false;
            } else if (appendResult) {
                this.isLoadingInline = false;
            } else {
                this.$loading.hide();
            }
        }, 300),

        async load(page = 1, appendResult = false) {
            let columns = [];
            let relations = [];

            if (!this.columns.hasOwnProperty('id')) {
                columns.push('id');
            }

            const appendedColumns = [];
            const countColumns = {};

            for(const column in this.columns) {
                if (this.columns.hasOwnProperty(column)) {
                    if (typeof this.columns[column] === 'string') {
                        columns.push(column);
                    } else {
                        if (this.columns[column].appends) {
                            appendedColumns.push(column);
                            continue;
                        }

                        if (this.columns[column].counts) {
                            const relationName = (this.columns[column].relation) || column.replace(/_count$/, '');
                            if (this.columns[column].query) {
                                countColumns[relationName] = this.columns[column].query;
                            } else {
                                countColumns[relationName] = null;
                            }
                            continue;
                        }

                        switch(this.columns[column].type) {
                            case 'RELATION':
                                // Remember that if we're loading a relation depending on another column, that column needs exist in the "select" as well
                                relations.push(column);
                                if (this.columns[column].hasOwnProperty('appendSelect')) {
                                    if (typeof this.columns[column].appendSelect === 'string') {
                                        columns.push(this.columns[column].appendSelect);
                                    } else if (this.columns[column].appendSelect === true) {
                                        columns.push(column + '_id');
                                    }
                                }
                                break;
                            case 'SLOTTED':
                                // Slotted fields expect the consumer of the component to render the column in the template,
                                // no value is provided to or from the API
                                break;
                            default:
                                columns.push(column);
                        }
                    }
                }
            }

            let params = {
                select: columns,
                page: page,
                per_page: this.perPage
            };

            if (relations.length) {
                params.with = relations;
            }

            if (appendedColumns.length) {
                params.appends = appendedColumns;
            }

            if (Object.keys(countColumns).length) {
                params.counts = countColumns;
            }

            if (this.searchQuery) {
                params.search = {
                    columns: this.searchColumns,
                    value: this.searchQuery,
                    split: this.splitSearchString
                };
            }

            if (Object.keys(this.orderBy).length) {
                params.order_by = {};
                for(const [column, order] of Object.entries(this.orderBy)) {

                    let sortColumn = column;
                    if (this.columns[column].hasOwnProperty('sortColumn')) {
                        sortColumn = this.columns[column].sortColumn;
                    }

                    if (
                        this.columns[column].hasOwnProperty('sortable') &&
                        this.columns[column].sortable === 'localized'
                    ) {
                        params.order_by[sortColumn] = 'localized' + order;
                    } else {
                        params.order_by[sortColumn] = order;
                    }
                }
            } else if (this.defaultOrderBy && Object.keys(this.defaultOrderBy).length) {
                params.order_by = this.defaultOrderBy;
            }

            if (this.customParams) {
                params = mergeWith(params, this.customParams, (currentValue, newValue, key) => {
                    switch (key) {
                        case 'select':
                        case 'with':
                        case 'appends':
                            if (!Array.isArray(currentValue)) {
                                currentValue = [];
                            }
                            return currentValue.concat(newValue);
                    }
                });
            }

            const rows = await this.$http.get(this.endpoint, {
                params: params
            }).then(response => {
                this.pagination.currentPage = response.data.data.current_page;
                this.pagination.lastPage = response.data.data.last_page;
                this.pagination.total = response.data.data.total;
                return response.data.data.data;
            }).catch(err => {
                console.error(err);
                return null;
            });

            if (appendResult) {
                this.rows = concat(this.rows, rows);
            } else {
                this.rows = rows;
            }

            this.$emit('loaded', rows);

            return rows;
        }
    }
};
