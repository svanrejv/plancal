import cloneDeep from 'lodash/cloneDeep';

export const addOrEditMixin = {
    inject: ['$http', '$i18n'],

    props: {
        id: String | Number,
        endpoint: String,
        eagerLoads: Array,
        eagerAppends: Array,
        pageTitle: String,
        modelNameSingular: String,
        backRoute: String,
        modelTitleField: {
            type: String,
            default: function() {
                return 'name';
            }
        },
        setModelOnUpdate: {
            type: Boolean,
            default: function() {
                return false;
            }
        },
        defaultModelData: {
            type: Object,
            default: function() {
                return {};
            }
        }
    },

    beforeMount() {
        this.loadDefinition();
    },

    data() {
        return {
            isInitializing: true,
            definition: null,
            invalid: true,
            model: cloneDeep(this.defaultModelData),
            errors: {},
            uploading: {}
        };
    },

    methods: {
        async loadDefinition() {
            this.$loading.show();
            this.isInitializing = true;

            await Promise.all([
                this.load(this.id),
                this.$http.get(this.endpoint + '/definition').then((res) => {
                    this.definition = this.beforeDefinitionSet(res.data.data);
                    return res.data.data;
                })
            ]).catch((err) => {
                console.error(err);
                this.$modal.show('error');
            });

            this.isInitializing = false;
            this.$loading.hide();
        },

        async load(id = null) {
            if (id) {
                let params = {};
                if (this.eagerLoads) {
                    params.with = this.eagerLoads;
                }
                if (this.eagerAppends) {
                    params.appends = this.eagerAppends;
                }
                return this.$http.get(this.endpoint + '/' + id, {
                    params: params
                }).then((res) => {
                    const data = this.beforeModelSet(res.data.data);
                    this.model = data;
                    return data;
                });
            } else {
                return Promise.resolve();
            }
        },

        beforeDefinitionSet(definition) {
            return definition;
        },

        beforeModelSet(data) {
            return data;
        },

        onSubmit(event) {
            if (!this.invalid) {
                this.submit();
            }
        },

        submit() {
            const data = cloneDeep(this.model);

            if (this.id || this.model.id) {
                this.submitEdit(data);
            } else {
                this.submitAdd(data);
            }
        },

        async waitForUpload() {
            if (Object.values(this.uploading).some(isUploading => isUploading)) {
                await (new Promise(resolve => setTimeout(resolve, 200)));
                await this.waitForUpload();
            }

            return true;
        },

        async submitAdd(data) {
            this.$loading.show();

            await this.waitForUpload();

            this.$http.post(this.endpoint, {
                data
            }).then((response) => {
                this.model = {};
                this.$loading.hide();
                this.$modal.show('success', null, this.$i18n.get('generic.model_saved', {model: this.modelNameSingular}), null, () => {
                    this.onAdd(data, ((response && response.data) ? response.data.data  : null));
                });
            }).catch((err) => {
                this.$loading.hide();
                this.onSubmitError(err);
            });
        },

        onAdd(model, respondedData) {
            this.$router.push({
                name: this.backRoute
            });
        },

        async submitEdit(data) {
            this.$loading.show();

            await this.waitForUpload();

            let params = {};
            if (this.setModelOnUpdate) {
                if (this.eagerLoads) {
                    params.with = this.eagerLoads;
                }
                if (this.eagerAppends) {
                    params.appends = this.eagerAppends;
                }
            }

            this.$http.patch(this.endpoint + '/' + (this.id || this.model.id), {
                data
            }, {
                params: params
            }).then((res) => {
                if (this.setModelOnUpdate) {
                    this.model = this.beforeModelSet(res.data.data);
                }
                this.$loading.hide();
                this.$toast.show(this.$i18n.get('generic.success'), this.$i18n.get('generic.model_saved', {model: this.modelNameSingular}));
                this.onEdit(this.model, res.data.data);
            }).catch((err) => {
                this.$loading.hide();
                this.onSubmitError(err);
            });
        },

        onEdit(model, respondedData) {},

        onSubmitError(error) {
            if (
                error.response &&
                error.response.status === 422 &&
                error.response.data.error_info.validation_errors &&
                Object.keys(error.response.data.error_info.validation_errors).length
            ) {
                this.$toast.show(error.response.data.error, this.$i18n.get('generic.model_not_saved', {model: this.modelNameSingular}), 'error');
                this.errors = error.response.data.error_info.validation_errors;
            } else {
                console.error(error);
                this.$modal.show('error');
            }
        },

        submitDelete(model = null) {
            this.$modal.show(
                'confirm',
                null,
                this.$i18n.get('generic.confirm_remove_model_message', {model: (model && model.name || this.model.name)}),
                null,
                null,
                () => {
                    this.delete((model && model.id) ? model.id : this.model.id);
                }
            );
        },

        async delete(modelId) {
            this.$loading.show();
            this.$http.delete(this.endpoint + '/' + modelId).then(() => {
                this.$loading.hide();
                this.onDelete(modelId);
            }).catch((err) => {
                console.error(err);
                this.$loading.hide();
                this.$modal.show('error');
            });
        },

        onDelete(modelId) {
            this.goBack(this.backRoute);
        }
    }
};
