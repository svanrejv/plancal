<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are generic view names that arent directly
    | related to a model
    |
    */

    'dashboard' => [
        'singular' => 'Dashboard',
        'plural' => 'Dashboards',
        'menu_title' => 'Dashboard',
        'menu_description' => 'Your dashboard'
    ],

    'store' => [
        'singular' => 'Store',
        'plural' => 'Stores',
        'menu_title' => 'Store',
        'menu_description' => 'Visit the store'
    ],

    'profile' => [
        'singular' => 'Profile',
        'plural' => 'Profiles',
        'menu_title' => 'Profile',
        'menu_description' => 'Change your profile'
    ],

    'notifications' => [
        'menu_description' => 'Happenings'
    ],

    'coins' => [
        'menu_description' => 'Coins in your wallet'
    ]

];
