<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the menu
    |
    */

    // Offline menu
    'play_offline' => 'Play offline',
    'play_offline_description' => 'Go to offline menu',
    'settings' => 'Settings',
    'settings_description' => 'Game settings',
    'exit_game' => 'Exit game',
    'exit_game_description' => 'Close the game',

    // Online menu
    'dashboard' => 'Dashboard',
    'dashboard_description' => 'Overview',
    'notifications' => 'Notifications',
    'notifications_description' => 'Happenings',
    'quick_round' => 'Quick round',
    'quick_round_description' => 'Browse or start your own',
    'events' => 'Events',
    'events_description' => 'Browse latest events',
    'games' => 'Games',
    'games_description' => 'Game history',
    'friends' => 'Friends',
    'friends_description' => 'Your list of friends',
    'logout' => 'Logout',
    'logout_description' => 'Logout from your account',

    // User menu
    'visit_the_store' => 'Visit the store',

    // In game menu
    'mulligan' => 'Mulligan',
    'mulligan_description' => 'Replay stroke',
    'concede' => 'Concede',
    'concede_description' => 'Resign round',
    'score' => 'Score',
    'score_description' => 'Display scorecard',
    'back_to_lobby' => 'Back to lobby',
    'back_to_lobby_description' => 'Exit round',
];
