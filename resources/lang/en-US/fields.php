<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic Field Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used globally throughout the application,
    | for different field types/labels
    |
    */

    'id' => 'ID',
    'email' => 'Email',
    'confirm_email' => 'Confirm email',
    'new_email' => 'New email',
    'confirm_new_email' => 'Confirm new email',
    'change_email' => 'Change email',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'new_password' => 'New password',
    'change_password' => 'Change password',
    'confirm_new_password' => 'Confirm new password',
    'name' => 'Name',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'display_name' => 'Display name',
    'avatar' => 'Avatar',
    'phone' => 'Phone',
    'address' => 'Address',
    'zip' => 'Zip',
    'city' => 'City',
    'country' => 'Country',
    'title' => 'Title',
    'entry_fee' => 'Entry fee',
    'sub_title' => 'Sub title',
    'description' => 'Description',
    'short_description' => 'Short description',
    'comment' => 'Comment',
    'expected_result' => 'Expected result',
    'body' => 'Body',
    'location' => 'Location',
    'image' => 'Image',
    'images' => 'Images',
    'cover_image' => 'Cover image',
    'badge' => 'Badge',
    'file' => 'File',
    'files' => 'Files',
    'attachment' => 'Attachment',
    'attachments' => 'Attachments',
    'from' => 'From',
    'to' => 'To',
    'role' => 'Role',
    'user' => 'User',
    'username' => 'Username',
    'order' => 'Order',
    'status' => 'Status',
    'locale' => 'Language',
    'country_code' => 'Country code',
    'identifier' => 'ID',
    'active' => 'Active',
    'category' => 'Category',
    'type' => 'Type',
    'slug' => 'Slug',
    'sub_actions' => 'Sub actions',
    'sort_by' => 'Sort by',
    'short_description' => 'Short description',

    'field_groups' => [
        'general' => 'General',
        'relations' => 'Relations'
    ],

    'descriptions' => [],

    'placeholders' => []

];
