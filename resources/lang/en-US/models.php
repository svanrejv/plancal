<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are generic model names
    |
    */

    'user' => [
        'singular' => 'User',
        'plural' => 'Users',
        'menu_title' => 'Users',
        'count' => '{0} :value users |{1} :value user|[2,*] :value users'
    ],
    'player' => [
        'singular' => 'Player',
        'plural' => 'Players',
        'menu_title' => 'Players',
        'count' => '{0} :value players |{1} :value player|[2,*] :value players'
    ],
    'member' => [
        'singular' => 'Member',
        'plural' => 'Members',
        'menu_title' => 'Members',
        'count' => '{0} :value members |{1} :value member|[2,*] :value members'
    ],
    'role' => [
        'singular' => 'Role',
        'plural' => 'Roles',
        'menu_title' => 'Roles',
        'count' => '{0} :value roles |{1} :value role|[2,*] :value roles'
    ],
    'notification' => [
        'singular' => 'Notification',
        'plural' => 'Notifications',
        'menu_title' => 'Notifications',
        'count' => '{0} :value notifications |{1} :value notification|[2,*] :value notifications'
    ],
    'course' => [
        'singular' => 'Course',
        'plural' => 'Courses',
        'menu_title' => 'Courses',
        'count' => '{0} :value courses |{1} :value course|[2,*] :value courses'
    ],
    'round' => [
        'singular' => 'Round',
        'plural' => 'Rounds',
        'menu_title' => 'Rounds',
        'count' => '{0} :value rounds |{1} :value round|[2,*] :value rounds'
    ],
    'hole' => [
        'singular' => 'Hole',
        'plural' => 'Holes',
        'menu_title' => 'Holes',
        'count' => '{0} :value holes |{1} :value hole|[2,*] :value holes'
    ],
    'stroke' => [
        'singular' => 'Stroke',
        'plural' => 'Strokes',
        'menu_title' => 'Strokes',
        'count' => '{0} :value strokes |{1} :value stroke|[2,*] :value strokes'
    ],
    'tournament' => [
        'singular' => 'Tournament',
        'plural' => 'Tournaments',
        'menu_title' => 'Tournaments',
        'count' => '{0} :value tournaments |{1} :value tournament|[2,*] :value tournaments'
    ],
    'wallet' => [
        'singular' => 'Wallet',
        'plural' => 'Wallets',
        'menu_title' => 'Wallet',
        'count' => '{0} :value wallets |{1} :value wallet|[2,*] :value wallets'
    ],
    'post' => [
        'singular' => 'Post',
        'plural' => 'Posts',
        'menu_title' => 'Posts',
        'count' => '{0} :value posts |{1} :value post|[2,*] :value posts'
    ],
    'comment' => [
        'singular' => 'Comment',
        'plural' => 'Comments',
        'menu_title' => 'Comments',
        'count' => '{0} :value comments |{1} :value comment|[2,*] :value comments'
    ],

];
