<?php

use App\Enums\BallType;

return [
    'ball_type' => [
        BallType::REGULAR => 'Regular',
        BallType::PRACTICE => 'Practice',
        BallType::FOAM => 'Foam'
    ],
];
