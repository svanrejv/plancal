<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used globally throughout the application,
    | without any specific context
    |
    */

    'private' => 'Private',
    'public' => 'Public',
    'yes' => 'Yes',
    'no' => 'No',
    'ok' => 'OK',
    '404' => 'Could not find what you were looking for',
    'settings' => 'Settings',
    'greetings' => 'Hello!',
    'greetings_user' => 'Hello :user!',
    'an_error_occurred' => 'An error occurred',
    'error_message' => 'Something went wrong, try again or contact support',
    'success' => 'Success',
    'loading' => 'Loading...',
    'no_result' => 'No result',
    'no_result_attr' => 'No :attribute',
    'are_you_sure' => 'Are you sure?',
    'model_saved' => ':model saved',
    'model_not_saved' => ':model not saved',
    'confirm_remove_model_message' => 'Do you want to permanently delete :model?',
    'search_query_too_short' => 'Please type more than 2 characters',
    'no_option' => 'None',
    'options' => 'Options',
    'created_by' => 'Created by',
    'created_at' => 'Created at',
    'created' => 'Created',
    'updated_at' => 'Updated at',
    'start_at' => 'Starts at',
    'end_at' => 'Ends at',
    'published_at' => 'Published at',
    'completed_at' => 'Completed at',
    'removed_attr' => 'Removed :attribute',
    'more' => 'More',
    'period' => 'Period',
    'period_from' => 'From',
    'period_to' => 'To',
    'menu' => 'Menu',
    'unknown' => 'Unknown',
    'coins' => 'coins',
    'cost_coins' => ':cost coins',
    'HCP' => 'HCP',

    'import' => [
        'info' => 'Download the example file and fill the first sheet with the data you want to import. All rows in the import needs to be valid for any rows to be saved.',
        'download_example' => 'Download example file',
        'import_failed' => 'Imported file contains errors',
        'import_failed_info' => 'Please correct the errors and upload a new file',
        'row_number' => 'Row #',
        'error_message' => 'Error message'
    ],

    'errors' => [
        'invalid_response_please_restart' => 'Invalid data received from server, please restart the game',
        'could_not_communicate_with_socket' => 'Could not communicate with game, you might be offline.'
    ]
];
