<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login' => 'Log in',
    'logout' => 'Log out',
    'logged_in_as' => 'Logged in as',
    'forgot_password' => 'Forgot your password?',
    'forgot_password_confirm_message' => 'Fill in your email to send a link to creating a new password',
    'reset_password' => 'Reset password',
    'reset_password_info' => 'Enter your new password below',
    'already_have_account' => 'Already have an account?',
    'no_account' => 'Don’t have an account?',
    'sign_up' => 'Sign up',

    'registration_success_title' => 'Almost done!',
    'registration_success_message' => 'We\'ve now sent an email to you with a link to verify your account',
    'registration_failed_title' => 'Registration failed',

    'actions' => [
        'login' => 'Log in',
        'sign_up' => 'Sign up',
        'forgot_password' => 'Forgot your password?',
    ],

    'errors' => [
        'user_not_found' => 'The user could not be found',
        'user_disabled' => 'The user account has been disabled',
        'invalid_credentials' => 'The credentials provided are not valid',
        'invalid_email' => 'The email address is badly formatted',
        'wrong_password' => 'The password is invalid or the user does not have a password',
        'weak_password' => 'The password must be 8 characters long or more',
        'email_already_in_use' => 'The email address is already in use by another account',
        'passwords_not_matching' => 'Passwords are not matching',
        'emails_not_matching' => 'Emails are not matching',
        'reset_password_email_failed' => 'Email could not be sent to this email address',
        'reset_password_failed' => 'Could not reset the password, please request a new link',
        'invalid_device_name' => 'Device name is missing or invalid',
        'invalid_token' => 'Invalid token',
        'no_game_id' => 'WARNING! No game-identifier found',
    ]

];
