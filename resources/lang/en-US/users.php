<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are related to user management and views
    |
    */

    'confirm_new_user_send_mail_title' => 'Notify the new user?',
    'confirm_new_user_send_mail_message' => 'Would you like to send an email to the new user about his new account and password?',
    'confirm_reset_password_title' => 'Reset password?',
    'confirm_reset_password_message' => 'Would you like to send an email to the user to reset password?',
    'removed_user' => 'Removed user',
    'import_users' => 'Importer users',
    'flag' => 'flag',

    'fields' => [
        'interface_settings' => 'Interface settings',
        'default_game_settings' => 'Default game settings',
        'default_player_settings' => 'Default player settings',
        'hcp' => 'Handicap',
        'hcp_diff' => 'Handicap differential'
    ],

    'player_settings' => [
        'hand' => 'Hand',
        'ball_type' => 'Ball type',
        'club_set' => 'Club set',
        '7_iron_distance' => '7 iron distance',
        'teeing_type' => 'Teeing type',
        'shot_shape' => 'Shot shape',
    ],

    'interface_settings' => [
        'unit_type' => 'Unit type'
    ],

    'validation_errors' => [
        'friendship_already_requested' => 'You have already requested friendship with this user',
        'no_pending_friendship' => 'There is no pending friend request with this user',
        'invalid_display_name' => 'Not valid - should be 3-20 characters and only numbers, letters, underscores and dashes.'
    ]
];
