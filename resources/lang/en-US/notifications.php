<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in notifications
    |
    */

    'greetings' => 'Hello!',
    'salutations' => 'Regards',
    'trouble_clicking_link' => 'If you’re having trouble clicking the ":actionText" button, copy and paste the URL below into your web browser: [:actionURL](:actionURL)',
    'copyright' => 'All rights reserved',

    'reset_password_email' => [
        'subject' => 'Reset Password Notification',
        'body' => 'You are receiving this email because we received a password reset request for your account.',
        'action_text' => 'Reset password',
        'expire_text' => 'This password reset link will expire in :minutes minutes',
        'explanation_text' => 'If you did not request a password reset, no further action is required.'
    ],

    'new_user_email' => [
        'subject' => 'New account',
        'body' => 'An account for :app_name has been created for you. Click the link below to create your password.'
    ],

    'friend_request' => [
        'pending' => [
            'title' => ':sender sent a friend request',
            'short_title' => 'Sent a friend request'
        ],
        'accepted' => [
            'title' => ':sender accepted your friend request',
            'short_title' => 'Accepted your friend request'
        ]
    ],

    'user_finished_round' => [
        'title' => ':user has finished the round',
        'body' => ':user has now finished the round ":round_title"'
    ],

    'round_result' => [
        'title' => 'Round ":round_title" complete',
        'body' => 'The round ":round_title" has now ended.'
    ],

    'round_invite' => [
        'pending' => [
            'title' => ':sender invited you to play a round of :game_mode',
            'short_title' => 'You\'ve been invited to play'
        ]
    ],

    'round_join' => [
        'title' => ':user joined a round of :game_mode',
        'body' => 'User :user joined a round of :game_mode'
    ],

    'round_invite_local' => [
        'pending' => [
            'title' => ':sender invited you to play a round of :game_mode locally',
            'short_title' => 'You\'ve been invited to play locally'
        ]
    ],

    'stroke_created' => [
        'title' => ':sender hit a stroke',
        'short_title' => 'Hit a stroke'
    ],

    'match_play_hole_score' => [
        'title' => 'Player :sender finished hole :hole',
        'winner_body' => ':sender finished hole :hole and :winner won the hole',
        'no_winner_body' => ':sender finished hole :hole in a tie',
        'current_leader' => 'The leader of the round is :leader with :max_points points over :min_points',
        'no_current_leader' => 'The round is currently tied'
    ],

    'match_play_round_result' => [
        'title' => 'Round ":round" finished',
        'winner_body' => 'The winner of the round was :winner with :max_points points over :min_points',
        'no_winner_body' => 'The round ended in a tie'
    ],

    'hole_score' => [
        'title' => 'Player :sender finished hole :hole with a :scoring_term_text',
        'body' => ':sender finished hole :hole with a :scoring_term_text'
    ],

    'hole_in_one' => [
        'title' => 'Amazing! Player :sender made a hole-in-one on hole :hole!',
        'body' => 'Amazing! Player :sender made a hole-in-one on hole :hole!'
    ],

    'game_version_update' => [
        'title' => 'Download instructions for golf game',
        'body' => 'Click the button to download the update to version :version',
        'action_text' => 'Download',
        'expire_text' => 'This download link will expire in :days days',
    ],

    'map_pack_version_update' => [
        'title' => 'Download instructions for map pack :title',
        'body' => 'Click the button to download the updated version :version',
        'action_text' => 'Download',
        'expire_text' => 'This download link will expire in :days days',
    ],

    'map_pack_download' => [
        'title' => 'Download instructions for map pack :title',
        'body' => 'Click the button to download the latest version',
        'action_text' => 'Download',
        'expire_text' => 'This download link will expire in :days days. If the link has expired you can request a new download link.',
    ],

    'wallet_updated' => [
        'title' => 'Your token balance has been updated',
        'body_credit' => 'Balance has been decreased by :amount (:action)',
        'body_debit' => 'Balance has been increased with :amount (:action)'
    ],

    'new_club_member' => [
        'pending' => [
            'title' => '[:club] :sender has requested to join your club!',
            'body' => ':sender has requested to join your club ":club"'
        ],
        'joined' => [
            'title' => '[:club] :sender has joined your club!',
            'body' => ':sender has joined your club ":club"'
        ]
    ],

    'club_role' => [
        'member' => [
            'title' => ':sender has added you to the club :club'
        ],
        'moderator' => [
            'title' => ':sender made you moderator of the club :club'
        ],
        'admin' => [
            'title' => ':sender made you admin of the club :club'
        ]
    ],

    'hcp_changed' => [
        'title' => 'You have a new handicap',
        'body' => 'Your new handicap is :hcp',
    ],

    'round_hcp_changed' => [
        'title' => 'Your handicap on round :round has med adjusted',
        'body' => 'Your new handicap is :hcp',
    ],
];
