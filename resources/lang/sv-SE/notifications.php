<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in notifications
    |
    */

    'greetings' => 'Hej!',
    'salutations' => 'Med vänlig hälsning',
    'trouble_clicking_link' => 'Om inte ":actionText" knappen fungerar, kopiera och klistra in nedan URL i din webbläsare: [:actionURL](:actionURL)',
    'copyright' => 'Alla rättigheter förbehålls',

    'reset_password_email' => [
        'subject' => 'Återställning av lösenord',
        'body' => 'Du får detta email för att någon har begärt återställning av ditt lösenord.',
        'action_text' => 'Återställ lösenord',
        'expire_text' => 'Länken är giltig i :minutes minuter.',
        'explanation_text' => 'Om du inte har begärt återställning av ditt lösenord så kan du ignorera detta email.'
    ],

    'new_user_email' => [
        'subject' => 'Nytt konto',
        'body' => 'Ett konto för :app_name har skapats åt dig. Klicka på länken nedan för att skapa ditt lösenord.'
    ],

    'friend_request' => [
        'pending' => [
            'title' => ':sender har skickat en vänförfrågan'
        ],
        'accepted' => [
            'title' => ':sender har accepterat din vänförfrågan'
        ]
    ],

];
