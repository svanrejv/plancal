<?php

return [

    /*
    |--------------------------------------------------------------------------
    | View Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are generic view names that arent directly
    | related to a model
    |
    */

    'dashboard' => [
        'singular' => 'Dashboard',
        'plural' => 'Dashboards',
        'menu_title' => 'Dashboard'
    ]

];
