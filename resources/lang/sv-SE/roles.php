<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Role Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for translating role names
    |
    */

    'admin' => 'Administratör',
    'user' => 'Användare'
];
