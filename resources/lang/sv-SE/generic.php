<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used globally throughout the application,
    | without any specific context
    |
    */

    'private' => 'Privat',
    'public' => 'Publik',
    'yes' => 'Ja',
    'no' => 'Nej',
    'ok' => 'OK',
    '404' => 'Kunde inte hitta det du letade efter',
    'settings' => 'Inställningar',
    'greetings' => 'Hej!',
    'greetings_user' => 'Hej :user!',
    'an_error_occurred' => 'Ett fel inträffade',
    'error_message' => 'Något gick fel, försök igen eller kontakta support',
    'success' => 'Lyckades',
    'loading' => 'Laddar...',
    'no_result' => 'Inget resultat',
    'no_result_attr' => 'Inga :attribute',
    'are_you_sure' => 'Är du säker?',
    'model_saved' => ':model sparad',
    'model_not_saved' => ':model kunde inte sparas',
    'confirm_remove_model_message' => 'Vill du ta bort :model permanent?',
    'search_query_too_short' => 'Var god skriv fler än 2 tecken',
    'no_option' => 'Inget',
    'options' => 'Alternativ',
    'created_by' => 'Skapad av',
    'reported_by' => 'Rapporterad av',
    'created_at' => 'Skapad',
    'updated_at' => 'Uppdaterad',
    'completed_at' => 'Utförd',
    'closed_at' => 'Stängd',
    'removed_attr' => 'Borttagen :attribute',
    'more' => 'Mer',
    'period' => 'Period',
    'period_from' => 'Från',
    'period_to' => 'Till',
    'distance' => 'Avstånd',
    'statistics' => 'Statistik',

    'import' => [
        'info' => 'Ladda ned exempelfilen och fyll i det första bladet med den data du vill importera. Alla rader måste gå igenom valideringen för att kunna spara.',
        'download_example' => 'Ladda ned exempelfil',
        'import_failed' => 'Den importerade filen innehåller fel',
        'import_failed_info' => 'Var god korrigera felen och ladda upp en ny fil',
        'row_number' => 'Rad #',
        'error_message' => 'Felmeddelande'
    ],
];
