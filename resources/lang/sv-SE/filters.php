<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic Filter Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used globally throughout the application,
    | for different filters
    |
    */

    'sort_by' => 'Sortering',
    'game_mode' => 'Spelläge',

];
