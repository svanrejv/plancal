<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Model Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are generic model names
    |
    */

    'user' => [
        'singular' => 'Användare',
        'plural' => 'Användare',
        'menu_title' => 'Användare'
    ],
    'role' => [
        'singular' => 'Roll',
        'plural' => 'Roller',
        'menu_title' => 'Roller'
    ],
    'notification' => [
        'singular' => 'Notifikation',
        'plural' => 'Notifikationer',
        'menu_title' => 'Notifikationer'
    ],
    'course' => [
        'singular' => 'Bana',
        'plural' => 'Banor',
        'menu_title' => 'Banor'
    ],
    'round' => [
        'singular' => 'Runda',
        'plural' => 'Rundor',
        'menu_title' => 'Rundor'
    ],
    'hole' => [
        'singular' => 'Hål',
        'plural' => 'Hål',
        'menu_title' => 'Hål'
    ],
    'stroke' => [
        'singular' => 'Slag',
        'plural' => 'Slag',
        'menu_title' => 'Slag'
    ],

];
