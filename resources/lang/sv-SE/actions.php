<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Actions Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used as generic actions
    |
    */

    'create_new' => 'Skapa nytt',
    'create_attr' => 'Skapa :attribute',
    'create_attr_in' => 'Skapa :attribute i :scope',
    'create_attr_for' => 'Skapa :attribute för :scope',
    'edit_attr' => 'Redigera :attribute',
    'edit_attr_in' => 'Redigera :attribute i :scope',
    'edit_attr_for' => 'Redigera :attribute för :scope',
    'show_attr' => 'Visa :attribute',
    'show' => 'Visa',
    'report_attr' => 'Rapportera :attribute',
    'hide' => 'Göm',
    'add' => 'Lägg till',
    'add_and_create_new' => 'Lägg till & Skapa ny',
    'save' => 'Spara',
    'send' => 'Skicka',
    'proceed' => 'Fortsätt',
    'copy' => 'Kopiera',
    'register' => 'Registrera',
    'remove' => 'Ta bort',
    'delete' => 'Delete',
    'select' => 'Välj',
    'cancel' => 'Avbryt',
    'clear' => 'Rensa',
    'close' => 'Stäng',
    'leave' => 'Lämna',
    'back' => 'Gå tillbaka',
    'search' => 'Sök',
    'confirm' => 'Bekräfta',
    'confirm_image_remove' => 'Är du säker att du vill ta bort denna bild?',
    'confirm_file_remove' => 'Är du säker att du vill ta bort denna fil?',
    'change' => 'Ändra',
    'edit' => 'Redigera',
    'update' => 'Uppdatera',
    'load_more' => 'Ladda mer',
    'load_older' => 'Ladda äldre',
    'select_date' => 'Välj datum',
    'add_comment' => 'Kommentera',
    'assign' => 'Tilldela',
    'add_file' => 'Lägg till fil',
    'add_files' => 'Lägg till filer',
    'add_image' => 'Lägg till bild',
    'add_images' => 'Lägg till bilder',
    'go_home' => 'Gå till startsidan',
    'select_country' => 'Välj land',
    'switch' => 'Byt',
    'filter' => 'Filtrera',
    'filter_on_attr' => 'Filtrera på :attribute',
    'select_attr' => 'Välj :attribute',
    'choose_attr' => 'Välj :attribute',
    'import' => 'Importera',
    'group_by' => 'Gruppera på',
    'mark_as_done' => 'Markera som klar',
    'mark_as_read' => 'Markera som läst'

];
