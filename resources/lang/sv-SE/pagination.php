<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'previous' => 'Föregående',
    'next'     => 'Nästa',
    'first'     => 'Första',
    'last'     => 'Sista',
    'showing_count_of_total' => 'Visar :count av :total totalt'

];
