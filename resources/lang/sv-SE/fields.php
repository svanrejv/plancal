<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Generic Field Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used globally throughout the application,
    | for different field types/labels
    |
    */

    'id' => 'ID',
    'email' => 'Email',
    'confirm_email' => 'Bekräfta email',
    'new_email' => 'Ny email',
    'confirm_new_email' => 'Bekräfta ny email',
    'password' => 'Lösenord',
    'confirm_password' => 'Bekräfta lösenord',
    'new_password' => 'Nytt lösenord',
    'confirm_new_password' => 'Bekräfta nytt lösenord',
    'name' => 'Namn',
    'first_name' => 'Förnamn',
    'last_name' => 'Efternamn',
    'phone' => 'Telefon',
    'address' => 'Adress',
    'zip' => 'Postnummer',
    'city' => 'Ort',
    'country' => 'Land',
    'title' => 'Rubrik',
    'sub_title' => 'Underrubrik',
    'description' => 'Beskrivning',
    'body' => 'Innehåll',
    'location' => 'Plats',
    'image' => 'Bild',
    'images' => 'Bilder',
    'file' => 'Fil',
    'files' => 'Filer',
    'attachment' => 'Bilaga',
    'attachments' => 'Bilagor',
    'from' => 'Från',
    'to' => 'Till',
    'role' => 'Roll',
    'user' => 'Användare',
    'order' => 'Ordning',
    'status' => 'Status',
    'locale' => 'Språk',
    'identifier' => 'ID',
    'active' => 'Aktiv',
    'category' => 'Kategori',
    'type' => 'Typ',
    'slug' => 'Slug',
    'sub_actions' => 'Underalternativ',

    'field_groups' => [
        'general' => 'Allmänt',
        'relations' => 'Relationer',
        'features' => 'Funktioner'
    ],

    'descriptions' => [],

    'placeholders' => []

];
