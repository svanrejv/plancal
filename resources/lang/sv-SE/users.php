<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are related to user management and views
    |
    */

    'confirm_new_user_send_mail_title' => 'Notifiera ny användare?',
    'confirm_new_user_send_mail_message' => 'Vill du skicka ett email till användaren om att ett konto skapats?',
    'confirm_reset_password_title' => 'Återställ lösenord?',
    'confirm_reset_password_message' => 'Vill du skicka ett mail till användaren för att återställa sitt lösenord?',
    'removed_user' => 'Borttagen användare',
    'import_users' => 'Importera användare',

    'fields' => [
        'interface_settings' => 'Interface settings',
        'default_game_settings' => 'Default game settings',
        'default_player_settings' => 'Default player settings'
    ],

    'player_settings' => [
        'hand' => 'Hand',
        'ball_type' => 'Bolltyp',
        'club_set' => 'Klubbtyp',
        '7_iron_distance' => '7 iron distans',
        'teeing_type' => 'Teeing typ',
        'shot_shape' => 'Skott typ',
    ],

    'interface_settings' => [
        'unit_type' => 'Enhet'
    ],

    'validation_errors' => [
        'friendship_already_requested' => 'Du har redan skickat en vänförfrågan till denna användaren',
        'no_pending_friendship' => 'Det finns ingen vänförfrågan till denna användaren'
    ]
];
