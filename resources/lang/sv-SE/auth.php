<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Uppgifterna du angav är felaktiga',
    'throttle' => 'För många inloggninsförsök. Försök igen om :seconds sekunder.',
    'login' => 'Logga in',
    'logout' => 'Logga ut',
    'logged_in_as' => 'Inloggad som',
    'forgot_password' => 'Glömt lösenordet?',
    'forgot_password_confirm_message' => 'Fyll i din email för att skicka en länk till att skapa ett nytt lösenord',
    'reset_password' => 'Återställ lösenord',
    'reset_password_info' => 'Fyll i ditt nya lösenord nedan',
    'no_account' => 'Har du inget konto?',
    'sign_up' => 'Bli medlem',

    'errors' => [
        'user_not_found' => 'Användaren kunde inte hittas',
        'user_disabled' => 'Användaren är inaktiverad',
        'invalid_credentials' => 'Uppgifterna du angav är inte giltiga',
        'invalid_email' => 'Email-adressen är inte giltig',
        'wrong_password' => 'Lösenordet är felaktigt',
        'weak_password' => 'Lösenordet måste vara 8 tecken eller längre',
        'email_already_in_use' => 'Email-adressen är används redan',
        'passwords_not_matching' => 'Lösenorden matchar inte',
        'emails_not_matching' => 'Email-fälten matchar inte',
        'reset_password_email_failed' => 'Email kunde inte skickas till angiven adress',
        'reset_password_failed' => 'Kunde inte återställa lösenordet, var god begär en ny länk',
        'invalid_device_name' => 'Enhetsnamnet saknas eller är ogiltigt',
        'invalid_token' => 'Ogiltig autentiseringstoken'
    ]

];
