import i18n from '../../../shared/js/Helpers/i18n';
import useAuthStore from '../../../shared/js/Stores/auth';

export default [
    {
        path: '/companies',
        name: 'companies',
        component: () => import(
            /* webpackPrefetch: true */
            '../Views/ListViewGeneric'
        ),
        props: () => {
            const auth = useAuthStore();
            return {
                endpoint: 'companies',
                pageTitle: i18n.get('models.company.menu_title'),
                modelNameSingular: i18n.get('models.company.singular'),
                modelTitleField: 'name',
                searchColumns: 'name',
                listColumns: {
                    name: i18n.get('fields.name'),
                    contact_person: i18n.get('fields.contact_person'),
                    email: i18n.get('fields.email'),
                    phone: i18n.get('fields.phone'),
                },
                addRouteName: 'add-company',
                editRouteName: 'edit-company',
                actions: {
                    add: auth.userCanAny([
                        'manage-companies'
                    ]),
                    edit: auth.userCanAny([
                        'manage-companies'
                    ]),
                    delete: auth.userCanAny([
                        'manage-companies'
                    ])
                }
            };
        },
        meta: {
            title: () => {
                return i18n.get('models.company.menu_title');
            },
            auth: () => {
                const auth = useAuthStore();
                return auth.userCanAny(['manage-companies']);
            }
        }
    },
    {
        path: '/companies/add',
        name: 'add-company',
        component: () => import(
            /* webpackPrefetch: true */
            '../Views/AddOrEditGeneric'
        ),
        props: (route) => ({
            endpoint: 'companies',
            pageTitle: i18n.get('actions.create_attr', {
                attribute: i18n.get('models.company.singular')
            }),
            modelNameSingular: i18n.get('models.company.singular'),
            backRoute: 'companies'
        }),
        meta: {
            title: () => {
                return i18n.get('actions.create_attr', {
                    attribute: i18n.get('models.company.singular').toLowerCase()
                });
            },
            auth: () => {
                const auth = useAuthStore();
                return auth.userCanAny(['manage-companies']);
            }
        }
    },
    {
        path: '/companies/:id',
        name: 'edit-company',
        component: () => import(
            /* webpackPrefetch: true */
            '../Views/AddOrEditGeneric'
        ),
        props: (route) => ({
            id: route.params.id,
            endpoint: 'companies',
            pageTitle: i18n.get('actions.edit_attr', {
                attribute: i18n.get('models.company.singular')
            }),
            modelTitleField: 'title',
            modelNameSingular: i18n.get('models.company.singular'),
            backRoute: 'companies'
        }),
        meta: {
            title: () => {
                return i18n.get('actions.edit_attr', {
                    attribute: i18n.get('models.company.singular').toLowerCase()
                });
            },
            auth: () => {
                const auth = useAuthStore();
                return auth.userCanAny(['manage-companies']);
            }
        }
    }
];
