import { createApp } from 'vue';
import { createPinia } from 'pinia';
import axios from 'axios';
import Routes from './routes.js';
import * as Sentry from '@sentry/vue';
import { BrowserTracing } from '@sentry/tracing';
import { ExtraErrorData as ExtraErrorDataIntegration } from '@sentry/integrations';

import i18n from '../../shared/js/Helpers/i18n';

import ModalService from '../../shared/js/Plugins/ModalService';
import LoaderService from '../../shared/js/Plugins/LoaderService';
import ToastService from '../../shared/js/Plugins/ToastService';
import SocketService from '../../shared/js/Plugins/SocketService';
import EventService from '../../shared/js/Plugins/EventService';

import '../../shared/js/Middleware/VersionHandler';
import '../../shared/js/Middleware/QueryStringParser';
import '../../shared/js/Middleware/TokenHandler';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Accept'] = 'application/json; chartset=UTF-8';
axios.defaults.baseURL = process.env.MIX_API_URL;
axios.defaults.withCredentials = true;

import App from './Views/App';
const app = createApp(App);

app.config.globalProperties.$i18n = i18n;
app.provide('$i18n', i18n);
app.config.globalProperties.$http = axios;
app.provide('$http', axios);

app.use(createPinia());
app.use(Routes);
app.use(ModalService);
app.use(ToastService);
app.use(LoaderService);
app.use(SocketService);
app.use(EventService);

if (process.env.MIX_WEB_SENTRY_DSN) {
    Sentry.init({
        app,
        dsn: process.env.MIX_WEB_SENTRY_DSN,
        environment: process.env.MIX_APP_ENV,
        release: 'optishot-web@' + process.env.MIX_APP_VERSION,
        integrations: [
            new BrowserTracing({
                routingInstrumentation: Sentry.vueRouterInstrumentation(Routes),
                tracingOrigins: [
                    'localhost',
                    process.env.MIX_API_URL,
                    process.env.MIX_GAME_URL,
                    process.env.MIX_WEB_URL,
                    /^\//
                ],
            }),

            // Limit of how deep the object serializer should go. Anything deeper than limit will
            // be replaced with standard Node.js REPL notation of [Object], [Array], [Function] or
            // a primitive value. Defaults to 3.
            // When changing this value, make sure to update `normalizeDepth` of the whole SDK
            // to `depth + 1` in order to get it serialized properly
            // https://docs.sentry.io/platforms/javascript/configuration/options/#normalize-depth
            new ExtraErrorDataIntegration({
                depth: 10
            })
        ],
        tracesSampleRate: 0.2,
        logErrors: true,

        initialScope: {
            tags: {
                server: process.env.MIX_SENTRY_SERVER_NAME
            }
        },

        normalizeDepth: 11
    });

    Sentry.setContext('app', {
        version: process.env.MIX_APP_VERSION,
        server_name: process.env.MIX_SENTRY_SERVER_NAME
    });
}

app.mount('#app');

export default app;
