import { createRouter, createWebHistory } from 'vue-router';
import setDocumentTitle from '../../shared/js/Helpers/setDocumentTitle';
import i18n from '../../shared/js/Helpers/i18n';
import useProtocolStore from '../../shared/js/Stores/protocol';
import useAuthStore from '../../shared/js/Stores/auth';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import(
                /* webpackPrefetch: true */
                './Views/Home'
            ),
            meta: {
                title: () => {
                    return i18n.get('views.dashboard.menu_title');
                },
                auth: true
            }
        },

        {
            path: '/profile',
            name: 'profile',
            component: () => import('./Views/Profile'),
            props: true,
            meta: {
                title: () => {
                    return i18n.get('views.profile.menu_title');
                },
                auth: true
            }
        },

        {
            path: '/login',
            name: 'login',
            component: () => import('./Views/Login')
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('./Views/Register'),
            props: true
        },

        /**
         * Fallback
         */
        {
            path: '/:pathMatch(.*)',
            name: '404',
            component: () => import('./Views/NotFound')
        }
    ]
});

/**
 * Check authentication and authorization
 */
router.beforeEach((to, from, next) => {
    const protocol = useProtocolStore();

    if (to.meta && to.meta.auth) {
        const authStore = useAuthStore();
        authStore.init().then(async user => {
            await protocol.loadEnums();

            if (!await authStore.isLoggedIn()) {
                next({
                    name: 'login'
                });

                return;
            }

            if (typeof to.meta.auth === 'boolean') {
                if (to.meta.auth) {
                    next();
                } else {
                    next(false);
                }
            } else if (typeof to.meta.auth === 'function') {
                if (to.meta.auth(to)) {
                    next();
                } else {
                    next(false);
                }
            } else if (to.meta.auth) {
                if (authStore.userCanAll(to.meta.auth)) {
                    next();
                } else {
                    next(false);
                }
            } else {
                next();
            }
        });
    } else {
        protocol.loadEnums().then(() => {
            next();
        });
    }
});

/**
 * Set document title to the current page
 */
router.afterEach((to, from) => {
    const title = (to.meta && typeof to.meta.title === 'function') ? to.meta.title() : null;
    setDocumentTitle(title);
});

export default router;
