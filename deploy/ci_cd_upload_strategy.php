<?php

namespace Deployer;

set('shared_files', ['.env', 'firebase-credentials.json']);

set('upload_options', function () {
    $options = [
        '--exclude=.git',
        '--exclude=node_modules',
        '--exclude=/.phpstorm.meta.php',
        '--exclude=/_ide_helper.php',
        '--exclude=/_ide_helper_models.php',
        '--exclude=/.gitlab-ci.yml',
        '--exclude=/.eslintrc.json',
        '--exclude=/.styleci.yml',
        '--exclude=/.editorconfig',
        '--exclude=/.gitattributes',
        '--exclude=/.gitignore',
        '--exclude=/.env',
        '--exclude=/.env.example',
        '--exclude=/.env.build.staging',
        '--exclude=/.env.build.production',
        '--exclude=/webpack.mix.js',
        '--exclude=/package.json',
        '--exclude=/package-lock.json',
        '--exclude=/phpcs.xml',
        '--exclude=/phpstan.neon',
        '--exclude=/socket.plancal.local',
        '--exclude=/deploy.yaml',
        '--exclude=deploy'
    ];
    return compact('options');
});

desc('Upload application built on gitlab to your hosts');
task('gitlab:upload', function () {
    $localPath = __DIR__ . '/..';
    $configs = array_merge_recursive(get('upload_options'), [
        'options' => ['--delete']
    ]);
    upload($localPath, '{{release_path}}', $configs);
});
