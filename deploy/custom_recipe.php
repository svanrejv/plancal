<?php

namespace Deployer;

task('artisan:translations:clear', function () {
    run('cd {{release_path}} && php artisan translations:clear');
});

task('artisan:protocol:clear', function () {
    run('cd {{release_path}} && php artisan protocol:clear');
});

task('artisan:abilities:generate', function () {
    run('cd {{release_path}} && php artisan abilities:generate --force');
});

task('custom-artisan:horizon:purge', function () {
    $host = currentHost()->getAlias();
    if ($host === 'testing') {
        run('cd {{release_path}} && sudo -u www-data php artisan horizon:purge');
    } else {
        run('cd {{release_path}} && php artisan horizon:purge');
    }
});

task('artisan:websockets:restart', function () {
    run('cd {{release_path}} && php artisan websockets:restart');
});

task('composer:optimize', function () {
    run('cd {{release_path}} && composer dump-autoload --optimize');
});

task('custom-artisan:horizon:terminate', function () {
    $host = currentHost()->getAlias();
    if ($host === 'testing') {
        run('cd {{release_path}} && sudo -u www-data php artisan horizon:terminate');
    } else {
        run('cd {{release_path}} && php artisan horizon:terminate');
    }
});

task('custom-fpm:reload', function () {
    $host = currentHost()->getAlias();
    if ($host === 'staging') {
        run('sudo /bin/reload_phpfpm.sh');
    }
});
