<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class NewUserNotification extends ResetPassword implements ShouldQueue
{
    use Queueable;

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $link = url('/reset-password/' . $this->token . '?email=' . $notifiable->email);
        return (new MailMessage())
            ->subject(Lang::get('notifications.new_user_email.subject'))
            ->line(Lang::get('notifications.new_user_email.body', ['app_name' => config('app.name')]))
            ->action(Lang::get('notifications.reset_password_email.action_text'), $link)
            ->line(Lang::get(
                'notifications.reset_password_email.expire_text',
                ['minutes' => config('auth.passwords.users.expire')]
            ));
    }
}
