<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class ResetPasswordNotification extends ResetPassword implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        if ($notifiable->disabled_at) {
            return [];
        }

        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        $link = url('/reset-password/' . $this->token . '?email=' . $notifiable->email);
        return (new MailMessage())
            ->subject(Lang::get('notifications.reset_password_email.subject'))
            ->line(Lang::get('notifications.reset_password_email.body'))
            ->action(Lang::get('notifications.reset_password_email.action_text'), $link)
            ->line(Lang::get(
                'notifications.reset_password_email.expire_text',
                ['minutes' => config('auth.passwords.users.expire')]
            ))
            ->line(Lang::get('notifications.reset_password_email.explanation_text'));
    }
}
