<?php

namespace App\Notifications\Traits;

use App\Models\User;
use Illuminate\Notifications\Messages\BroadcastMessage;

trait HasBroadcastMessage
{
    public function toBroadcast($notifiable): BroadcastMessage
    {
        $data = $this->toArray($notifiable);

        if (
            !isset($data['sender_uuid'], $this->sender) ||
            !$this->sender instanceof User
        ) {
            return new BroadcastMessage($data);
        }

        User::mergeAppends(['avatar']);
        $data['sender'] = $this->sender->toArray();

        return new BroadcastMessage($data);
    }

    public function broadcastType(): string
    {
        return class_basename($this);
    }
}
