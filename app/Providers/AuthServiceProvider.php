<?php

namespace App\Providers;

use App\Helpers\JWT;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    public function boot(JWT $jwt): void
    {
        $this->registerPolicies();

        Auth::viaRequest('firebase', static function (Request $request) use ($jwt) {
            if ($request->header('Authorization')) {
                return $jwt->getAuthenticatedUser($request);

            }

            return null;
        });
    }
}
