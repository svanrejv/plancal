<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Horizon\Horizon;
use Laravel\Horizon\HorizonApplicationServiceProvider;

class HorizonServiceProvider extends HorizonApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        parent::boot();
        Horizon::night();
    }

    /**
     * Register the Horizon gate.
     *
     * This gate determines who can access Horizon in non-local environments.
     *
     * @return void
     */
    protected function gate(): void
    {
        Gate::define('viewHorizon', function ($user = null) {
            $allowedIps = [
                '83.241.142.178',
                '178.31.196.48',
                '217.25.34.166',
                '2.248.77.165'
            ];

            $ip = ($_SERVER['HTTP_X_REAL_IP']) ?? request()->ip();

            return (in_array($ip, $allowedIps, true));
        });
    }
}
