<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Role;
use Bouncer;
use Illuminate\Support\ServiceProvider;

class BouncerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Bouncer::useUserModel(User::class);
        Bouncer::useRoleModel(Role::class);
        Bouncer::cache();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }
}
