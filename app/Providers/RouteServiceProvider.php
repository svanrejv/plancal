<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';
    /**
     * @var string Web routes namespace
     */
    protected string $webNamespace = 'App\Http\Controllers\Web';
    /**
     * @var string API routes base namespace
     */
    protected string $apiNamespace = 'App\Http\Controllers\Api';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureRateLimiting();
        $this->routes(function () {
            Route::domain(config('app.api_domain'))
                ->prefix('v1')
                ->middleware('api')
                ->namespace($this->apiNamespace . '\V1')
                ->group(base_path('routes/api_v1.php'));

            Route::domain(config('app.web_domain'))
                ->middleware('web')
                ->namespace($this->webNamespace)
                ->group(base_path('routes/web.php'));
        });
    }
    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(config('app.rate_limit'))->by(Auth::id() ?: $request->ip());
        });
    }
}
