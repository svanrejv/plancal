<?php

namespace App\Contracts;

interface UsesSocketNotifications
{
    public function toSocket($notifiable): array;
}
