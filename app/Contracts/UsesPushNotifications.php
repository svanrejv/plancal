<?php

namespace App\Contracts;

interface UsesPushNotifications
{
    public function toPush($notifiable): array;
}
