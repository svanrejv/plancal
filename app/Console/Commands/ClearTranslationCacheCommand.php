<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ClearTranslationCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear cache for translations';

    /**
     * Execute the console command
     */
    public function handle(): void
    {
        Cache::tags('translations')->flush();
        $this->info('Translation cache cleared');
    }
}
