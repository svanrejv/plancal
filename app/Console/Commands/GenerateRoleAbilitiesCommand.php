<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateRoleAbilitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abilities:generate {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    // phpcs:ignore Generic.Files.LineLength.TooLong
    protected $description = 'Will seed the database with all role abilities defined in the abilities config (wrapper for db:seed)';

    /**
     * Execute the console command
     */
    public function handle(): void
    {
        if (
            $this->option('force') ||
            // phpcs:ignore Generic.Files.LineLength.TooLong
            $this->confirm('This will update your database to match abilities and roles configured in your abilities config, it will also update all users to match. Proceed?')
        ) {
            $this->call('db:seed', [
                '--class' => 'BouncerSeeder',
                '--force' => true
            ]);
        }
    }
}
