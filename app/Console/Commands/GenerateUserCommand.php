<?php

namespace App\Console\Commands;

use App\Models\User;
use Bouncer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Kreait\Laravel\Firebase\Facades\Firebase;

class GenerateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will create a new user in DB';

    /**
     * Execute the console command
     */
    public function handle(): void
    {
        $this->generateUser();
    }

    private function generateUser()
    {
        $this->line('--');
        $this->info('Please fill in user details');

        $firstName = $this->ask('First name');
        $lastName = $this->ask('Last name');
        $displayName = $this->ask('Username');
        $email = $this->ask('Email');
        $locale = $this->choice('Locale', array_keys(config('app.supported_locales')), '0');
        $role = $this->choice('Role', array_keys(config('abilities.registry.roles')), '0');
        $password = $this->ask('Password');

        if (!isset($firstName, $lastName, $displayName, $email, $password)) {
            $this->error('All details must be provided to continue.');
            return $this->generateUser();
        }

        try {
            (Validator::make([
                'password' => $password
            ], [
                'password' => 'required|min:6'
            ]))->validate();
        } catch (ValidationException $e) {
            $this->error('Password must be at least 6 characters');
            return $this->generateUser();
        }

        $user = new User();
        $user->compileDefinition();
        $user->fill([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'display_name' => $displayName,
            'email' => $email,
            'locale' => $locale,
            'country_code' => 'US',
        ]);

        $auth = Firebase::auth();
        $fbUser = $auth->createUser([
            'email' => $user->email,
            'password' => $password,
            'disabled' => false,
            'emailVerified' => true
        ]);

        $user->uuid = $fbUser->uid;

        try {
            if (!$user->save()) {
                throw new Exception('Could not save user');
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
            die();
        }

        Bouncer::assign($role)->to($user);
        Bouncer::refreshFor($user);

        $this->info('User successfully generated');
    }
}
