<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AdapterInterface;

class GenerateJsonTranslationsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate json file for all translations';

    /**
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * Execute the console command
     * @throws \JsonException
     */
    public function handle(): void
    {
        // phpcs:ignore Generic.Files.LineLength.TooLong
        if ($this->confirm('Do you want to generate json files for all currently available translations? This will overwrite local files')) {
            $supportedLanguages = config('app.supported_locales');
            $translations = [];

            foreach ($supportedLanguages as $key => $lang) {
                $translations[$key] = $this->compileLanguage($lang['dir']);
            }

            file_put_contents(
                $this->files()->getPathPrefix() . '/translations.json',
                json_encode($translations, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE)
            );
        }
    }

    /**
     * Compiles all available translation strings from given language
     *
     * @param string $lang
     *
     * @return array
     */
    private function compileLanguage(string $lang): array
    {
        $strings = [];
        $prefix = $this->files()->getPathPrefix();
        foreach ($this->files()->listContents($lang) as $dir) {
            if (Str::contains($dir['path'], 'php')) {
                $key = str_replace(["$lang/", '.php'], '', $dir['path']);
                $strings[$key] = include($prefix . $dir['path']);
            }
        }

        return $strings;
    }


    private function files()
    {
        if ($this->adapter === null) {
            $this->adapter = new Local(base_path('resources/lang'));
        }

        return $this->adapter;
    }
}
