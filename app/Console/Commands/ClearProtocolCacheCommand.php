<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ClearProtocolCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'protocol:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear cache for protocol';

    /**
     * Execute the console command
     */
    public function handle(): void
    {
        Cache::tags('protocol')->flush();
        $this->info('Protocol cache cleared');
    }
}
