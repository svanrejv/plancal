<?php

namespace App\Helpers;

use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class Appendables
{
    public static function user(Request $request, Builder $query, string $relationPrefix = 'user')
    {
        if ($appends = $request->get('appends')) {
            if (is_string($appends)) {
                $appends = explode(',', $appends);
            }

            $userAppends = array_filter(
                (new User())->allowedApiAppends(),
                static fn($appendedAttribute) => in_array($relationPrefix . '.' . $appendedAttribute, $appends, true)
            );

            if (!empty($userAppends)) {
                if (in_array('avatar', $userAppends, true)) {
                    $query->with($relationPrefix . '.media');
                }

                User::mergeAppends($userAppends);
            }
        }
    }

    public static function course(Request $request, Builder $query, string $relationPrefix = 'course')
    {
        if ($appends = $request->get('appends')) {
            if (is_string($appends)) {
                $appends = explode(',', $appends);
            }

            $userAppends = array_filter(
                (new Course())->allowedApiAppends(),
                static fn($appendedAttribute) => in_array($relationPrefix . '.' . $appendedAttribute, $appends, true)
            );

            if (!empty($userAppends)) {
                if (in_array('images', $userAppends, true)) {
                    $query->with($relationPrefix . '.media');
                }

                Course::mergeAppends($userAppends);
            }
        }
    }
}
