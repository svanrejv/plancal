<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Kreait\Firebase\Exception\Auth\RevokedIdToken;
use Kreait\Laravel\Firebase\Facades\Firebase;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Lcobucci\JWT\Token;

class JWT
{
    protected static ?string $header = null;
    protected static ?User $user = null;
    protected static array $authValues = [];

    /**
     * Parses Authorization header and retrieves value from specified prefix
     *
     * @param Request $request
     * @param string  $prefix Authorization prefix to get value from eg. Bearer or Apikey
     *
     * @return bool|string
     */
    public static function getValue(Request $request, string $prefix)
    {
        $header = $request->header('Authorization');

        if (
            (self::$header === $header) &&
            isset(self::$authValues[$prefix])
        ) {
            return self::$authValues[$prefix];
        }

        if (self::$header !== $header) {
            self::$authValues = [];
            self::$user = null;
        }

        if ($header && preg_match('/' . $prefix . '\s*(.*)\b/i', $header, $matches)) {
            self::$header = $header;
            self::$authValues[$prefix] = $matches[1];

            return $matches[1];
        }

        return false;
    }

    public static function getAuthenticatedUser(Request $request): ?User
    {
        if (!self::$user) {
            $firebaseAuth = Firebase::auth();
            $token = self::getValue($request, 'Bearer');

            if (!$token) {
                throw new HttpException(401, 'Invalid token');
            }

            try {
                $verifiedIdToken = $firebaseAuth->verifyIdToken($token, false);
                assert($verifiedIdToken instanceof Token\Plain);
            } catch (\Throwable $e) {
                throw new HttpException(401, 'Invalid token: ' . $e->getMessage());
            }

            $uuid = $verifiedIdToken->claims()->get('sub');
            $user = User::where('uuid', $uuid)->firstOrFail();

            // Revoke check calls HTTP, and we dont want to do this on every request
            // so we limit this per user and only check if token is revoked once in a while

            // There's also a weird bug where a new user is triggered with revoked token,
            // so we'll wait for 15 minutes before start checking this
            if (
                !Cache::tags('token_revoked_checks')->has($uuid) &&
                $user->created_at->isBefore(Carbon::now()->subMinutes(15))
            ) {
                try {
                    $firebaseAuth->verifyIdToken($token, true, 300);
                } catch (\Throwable $e) {
                    throw new HttpException(401, 'Invalid token: ' . $e->getMessage());
                }

                Cache::tags('token_revoked_checks')
                     ->put(
                         $uuid,
                         Carbon::now('UTC')->timestamp,
                         Carbon::now()->addHour()
                     );
            }

            self::$user = $user;
        }

        return self::$user;
    }
}
