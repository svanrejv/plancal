<?php

namespace App\Helpers;

use App\Enums\GameMode;
use App\Enums\ScoringTerm;
use App\Exceptions\InvalidGameMode;
use App\Helpers\Handicap\HandicapScore;
use App\Models\Hole;
use App\Models\Round;
use App\Models\RoundUser;
use App\Models\Score;
use App\Models\Scorecard;
use App\Models\Stroke;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Scoring
{
    public static function calculateSingleScore(Stroke $stroke): array
    {
        switch ($stroke->round?->game_mode) {
            case GameMode::STROKE_PLAY:
                $scoreData = self::calculateStrokePlay($stroke);
                break;
            case GameMode::MATCH_PLAY:
                $scoreData = self::calculateMatchPlay($stroke);
                break;
            case GameMode::CLOSEST_TO_PIN:
                $scoreData = self::calculateClosestToPin($stroke);
                break;
            case GameMode::LONGDRIVE:
                $scoreData = self::calculateLongdrive($stroke);
                break;
            default:
                throw new InvalidGameMode();
        }

        return $scoreData;
    }

    private static function calculateStrokePlay(Stroke $stroke): array
    {
        $strokesOnHole = Stroke::where('user_id', $stroke->user_id)
                               ->where('round_id', $stroke->round_id)
                               ->where('hole_id', $stroke->hole_id)
                               ->count();

        return [
            'strokes' => $strokesOnHole,
            'score' => $strokesOnHole - $stroke->hole->par,
            'score_hcp' => ($stroke->round->is_using_handicap) ?
                self::getHcpScore($strokesOnHole, $stroke) :
                null
        ];
    }

    private static function calculateClosestToPin(Stroke $stroke): array
    {
        $strokesOnHole = Stroke::where('user_id', $stroke->user_id)
                               ->where('round_id', $stroke->round_id)
                               ->where('hole_id', $stroke->hole_id)
                               ->get();

        return [
            'strokes' => $strokesOnHole->count(),
            'score' => $strokesOnHole->min('stroke_result.distance_to_hole')
        ];
    }

    private static function calculateLongdrive(Stroke $stroke): array
    {
        $strokesOnHole = Stroke::where('user_id', $stroke->user_id)
                               ->where('round_id', $stroke->round_id)
                               ->where('hole_id', $stroke->hole_id)
                               ->get();

        return [
            'strokes' => $strokesOnHole->count(),
            'score' => $strokesOnHole->max('stroke_result.distance_to_hole')
        ];
    }

    private static function calculateMatchPlay(Stroke $stroke): array
    {
        $opponent = $stroke->round->users->where('id', '!=', $stroke->user_id)->first();
        $myStrokesOnHole = Stroke::where('user_id', $stroke->user_id)
            ->where('round_id', $stroke->round_id)
            ->where('hole_id', $stroke->hole_id)
            ->count();
        $opponentStrokesOnHole = Stroke::where('user_id', $opponent->id)
            ->where('round_id', $stroke->round_id)
            ->where('hole_id', $stroke->hole_id)
            ->count();

        return [
            'strokes' => $myStrokesOnHole,
            'score' => ($myStrokesOnHole >= $opponentStrokesOnHole) ? 0 : 1,
            'score_hcp' => ($stroke->round->is_using_handicap) ?
                self::getHcpScore($myStrokesOnHole, $stroke) :
                null
        ];
    }

    public static function getHcpScore(int $totalStrokes, Stroke $stroke): int
    {
        return HandicapScore::getHoleNetScore(
            $totalStrokes,
            HandicapScore::getNumberOfExtraStrokes(
                $stroke->round->course,
                RoundUser::where('round_id', $stroke->round_id)
                         ->where('user_id', $stroke->user_id)
                         ->firstOrFail()->settings['hcp']
            ),
            $stroke->hole_id
        );
    }

    /**
     * Returns the winning score on specified round+hole
     * Returns null if tied or no scores found.
     */
    public static function getWinningScore(Round $round, Hole $hole): ?Score
    {
        $top2scores = Score::whereIn('scorecard_id', $round->scorecards()->select('id'))
                    ->where('hole_id', $hole->id)
                    ->orderBy('score', self::getScoreOrder($round->game_mode))
                    ->limit(2)
                    ->get();

        if (
            $top2scores->isEmpty() ||
            $top2scores->first()?->score === $top2scores->last()?->score
        ) {
            return null;
        }

        return $top2scores->first();
    }

    /**
     * Returns the winning user on specified round+hole.
     * Returns null if tied or no scores/user found.
     */
    public static function getHoleWinner(Round $round, Hole $hole): ?User
    {
        return self::getWinningScore($round, $hole)?->scorecard?->user;
    }

    public static function getCurrentPosition(Scorecard $scorecard): int
    {
        if ($scorecard->total_score === null) {
            return 0;
        }

        $query = Scorecard::query()
                          ->where('round_id', $scorecard->round_id)
                          ->whereNotNull('total_score');

        switch ($scorecard->round?->game_mode) {
            case GameMode::STROKE_PLAY:
            case GameMode::CLOSEST_TO_PIN:
                return $query->where('total_score', '<', $scorecard->total_score)->count() + 1;
            case GameMode::MATCH_PLAY:
            case GameMode::LONGDRIVE:
                return $query->where('total_score', '>', $scorecard->total_score)->count() + 1;
            default:
                throw new InvalidGameMode();
        }
    }

    public static function getCurrentLeader(Round $round): ?Scorecard
    {
        switch ($round->game_mode) {
            case GameMode::STROKE_PLAY:
            case GameMode::CLOSEST_TO_PIN:
                return $round->currentLeaderAsc;
            case GameMode::MATCH_PLAY:
            case GameMode::LONGDRIVE:
                return $round->currentLeaderDesc;
            default:
                throw new InvalidGameMode();
        }
    }

    public static function getCurrentLeaderQuery(Round $round): HasOne
    {
        switch ($round->game_mode) {
            case GameMode::STROKE_PLAY:
            case GameMode::CLOSEST_TO_PIN:
                return $round->currentLeaderAsc();
            case GameMode::MATCH_PLAY:
            case GameMode::LONGDRIVE:
                return $round->currentLeaderDesc();
            default:
                throw new InvalidGameMode();
        }
    }

    public static function getScoreOrder(int $gameMode): string
    {
        return match ($gameMode) {
            GameMode::STROKE_PLAY, GameMode::CLOSEST_TO_PIN => 'asc',
            GameMode::MATCH_PLAY, GameMode::LONGDRIVE => 'desc',
            default => throw new InvalidGameMode(),
        };
    }

    public static function getInvertedScoreOrder(int $gameMode): string
    {
        return (self::getScoreOrder($gameMode) === 'asc') ? 'desc' : 'asc';
    }

    public static function getTiedLeadersQuery(Round $round): HasMany
    {
        $winningScore = $round->scorecards()
            ->orderBy('total_score', self::getScoreOrder($round->game_mode))
            ->limit(1)
            ->pluck('total_score');

        return $round->scorecards()->where('total_score', $winningScore);
    }

    public static function roundIsTied(Round $round): bool
    {
        $top2scorecards = $round->scorecards()
              ->orderBy('total_score', self::getScoreOrder($round->game_mode))
              ->limit(2)
              ->get();

        return $top2scorecards->isNotEmpty() &&
               $top2scorecards->first()?->total_score === $top2scorecards->last()?->total_score;
    }

    public static function getCutoffScore(Round $round, int $cutoff): ?int
    {
        $score = $round->scorecards()
                       ->whereNotNull('total_score')
                       ->orderBy('total_score', self::getScoreOrder($round->game_mode))
                       ->select('total_score')
                       ->distinct()
                       ->offset($cutoff - 1)
                       ->first();

        if (!$score) {
            $score = $round->scorecards()
                           ->whereNotNull('total_score')
                           ->orderBy('total_score', self::getInvertedScoreOrder($round->game_mode))
                           ->select('total_score')
                           ->first();
        }

        return $score?->total_score;
    }

    public static function getScoringTerm(Stroke $stroke): int
    {
        $strokesOnHole = $stroke->round->strokes()
            ->where('user_id', $stroke->user_id)
            ->where('hole_id', $stroke->hole_id)
            ->count();
        if ($strokesOnHole === 1) {
            return ScoringTerm::HOLE_IN_ONE;
        }
        $par = $stroke->hole->par;
        $termBasis = $strokesOnHole - $par;

        return match ($termBasis) {
            -4 => ScoringTerm::CONDOR,
            -3 => ScoringTerm::ALBATROSS,
            -2 => ScoringTerm::EAGLE,
            -1 => ScoringTerm::BIRDIE,
            0 => ScoringTerm::PAR,
            1 => ScoringTerm::BOGEY,
            2 => ScoringTerm::DOUBLE_BOGEY,
            3 => ScoringTerm::TRIPLE_BOGEY,
            4 => ScoringTerm::QUADRUPLE_BOGEY,
            default => ScoringTerm::OTHER
        };
    }
}
