<?php

namespace App\Helpers;

class Locale
{
    /**
     * Returns the full locale key if found in supported locales
     */
    public static function getQualifiedLocale(string $requestedLocale): string
    {
        $supportedLocales = config('app.supported_locales');
        if (array_key_exists($requestedLocale, $supportedLocales)) {
            return $requestedLocale;
        }

        foreach ($supportedLocales as $locale) {
            if ($locale['short_locale'] === $requestedLocale) {
                return $locale['full_locale'];
            }
        }

        return $requestedLocale;
    }
}
