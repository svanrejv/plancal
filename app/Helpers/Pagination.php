<?php

namespace App\Helpers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class Pagination
{
    /**
     * Adds pagination to a query builder instance based on request params
     *
     * @param Request          $request
     * @param Builder|Relation $query
     * @param array            $select
     *
     * @return LengthAwarePaginator|Builder[]|Collection<Model>
     */
    public static function addPagination(Request $request, $query, array $select = ['*'])
    {
        if (
            ($pagination = $request->input('paginate')) !== null &&
            ($pagination === false || $pagination === 'false' || $pagination === '0')
        ) {
            return $query->get();
        }

        $perPage = $request->input('per_page');

        return $query->paginate($perPage, $select)->appends($request->query());
    }
}
