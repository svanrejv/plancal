<?php

namespace App\Models\Traits;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait UsesAvatar
{
    public function getAvatarAttribute()
    {
        return $this->getMedia('avatar')->map(static fn(Media $image) => [
            'id' => $image->id,
            'name' => $image->name,
            'file_name' => $image->file_name,
            'mime_type' => $image->mime_type,
            'url' => $image->getFullUrl(),
            'sizes' => [
                'thumb' => $image->getFullUrl('thumb'),
                'image_sm' => $image->getFullUrl('image_sm'),
                'image_md' => $image->getFullUrl('image_md')
            ]
        ]);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 150, 150)
            ->performOnCollections('avatar')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('image_sm')
            ->width(250)
            ->height(250)
            ->performOnCollections('avatar')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('image_md')
            ->width(500)
            ->height(500)
            ->performOnCollections('avatar')
            ->keepOriginalImageFormat();
    }
}
