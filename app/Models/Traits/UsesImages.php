<?php

namespace App\Models\Traits;

use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait UsesImages
{
    public function getImagesAttribute()
    {
        return $this->getMedia('images')->transform(static function (Media $image) {
            return [
                'id' => $image->id,
                'name' => $image->name,
                'file_name' => $image->file_name,
                'mime_type' => $image->mime_type,
                'url' => $image->getFullUrl(),
                'sizes' => [
                    'thumb' => $image->getFullUrl('thumb'),
                    'image_sm' => $image->getFullUrl('image_sm'),
                    'image_md' => $image->getFullUrl('image_md')
                ]
            ];
        });
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('images');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
             ->fit(Manipulations::FIT_CROP, 150, 150)
             ->performOnCollections('images');

        $this->addMediaConversion('image_sm')
             ->width(500)
             ->height(750)
             ->performOnCollections('images');

        $this->addMediaConversion('image_md')
             ->width(1000)
             ->height(1500)
             ->performOnCollections('images');
    }
}
