<?php

namespace App\Models;

use Bjerke\Bread\Builder\DefinitionBuilder;
use Bjerke\Bread\Fields\IntField;
use Bjerke\Bread\Fields\TextField;
use Bjerke\Bread\Models\BreadModel;
use Illuminate\Support\Facades\Lang;

class Venue extends BreadModel
{
    protected function define(DefinitionBuilder $definition): DefinitionBuilder
    {
        return $definition->addFields([
            (new TextField('title'))
                ->label(Lang::get('fields.title'))
                ->required(true)
                ->addValidation('unique:venues,title' . (($this->exists) ? (',' . $this->id) : ''))
                ->minLength(3)
                ->maxLength(45),

            (new IntField('seats_count'))
                ->label(Lang::get('fields.seats_count'))
                ->min(0),

            (new TextField('address'))
                ->label(Lang::get('fields.address'))
                ->minLength(3)
                ->maxLength(45),

            (new TextField('postal code'))
                ->label(Lang::get('fields.postal code'))
                ->minLength(5)
                ->maxLength(7),

            (new TextField('city'))
                ->label(Lang::get('fields.city'))
                ->minLength(3)
                ->maxLength(45),

            (new IntField('ext_id'))
                ->label(Lang::get('fields.external_identifier'))
                ->min(0),
        ]);
    }

    public function setPostalCodeAttribute($postal_code)
    {
        $this->attributes['postal_code'] = preg_replace('/\D/','', $postal_code);
    }

}
