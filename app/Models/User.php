<?php

namespace App\Models;

use App\Models\Traits\UsesAvatar;
use Bjerke\Bread\Builder\DefinitionBuilder;
use Bjerke\Bread\Fields\BoolField;
use Bjerke\Bread\Fields\EmailField;
use Bjerke\Bread\Fields\ImageUploadField;
use Bjerke\Bread\Fields\SelectField;
use Bjerke\Bread\Fields\TextField;
use Bjerke\Bread\Models\BreadModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Laravel\Cashier\Billable;
use Silber\Bouncer\Database\Concerns\Authorizable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperUser
 */
class User extends BreadModel implements AuthenticatableContract, AuthorizableContract, HasLocalePreference, HasMedia
{
    use Authenticatable;
    use Authorizable;
    use Billable;
    use HasRolesAndAbilities;
    use Notifiable;
    use InteractsWithMedia;
    use UsesAvatar {
        UsesAvatar::registerMediaCollections insteadof InteractsWithMedia;
        UsesAvatar::registerMediaConversions insteadof InteractsWithMedia;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'media',
        'stripe_id',
        'pm_type',
        'pm_last_four',
        'trial_ends_at',
    ];

    protected $dates = [
        'disabled_at'
    ];

    /**
     * Overrides the default prevention of returning sensitive data
     * if logged in user is not current model instance.
     *
     * @var bool
     */
    public bool $allowSensitive = false;

    protected function define(DefinitionBuilder $definition): DefinitionBuilder
    {
        $localeOptions = [];
        foreach (config('app.supported_locales') as $locale) {
            $localeOptions[$locale['full_locale']] = $locale['name'];
        }

        $roleOptions = [];
        Role::all()->each(static function ($role) use (&$roleOptions) {
            if ($role instanceof Role) {
                $roleOptions[$role->name] = Lang::get('roles.' . $role->name);
            }
        });

        return $definition->addFields([
            (new TextField('first_name'))->label(Lang::get('fields.first_name'))->required(true),

            (new TextField('last_name'))->label(Lang::get('fields.last_name'))->required(true),

            (new TextField('display_name'))
                ->label(Lang::get('fields.display_name'))
                ->required(true)
                ->addValidation('alpha_dash|min:3|max:20')
                ->addValidation('unique:users,display_name' . (($this->exists) ? (',' . $this->id) : '')),

            (new ImageUploadField('avatar'))
                ->multiple(false)
                ->label(Lang::get('fields.avatar')),

            (new EmailField('email'))
                ->label(Lang::get('fields.email'))
                ->required(true)
                ->addValidation('unique:users,email' . (($this->exists) ? (',' . $this->id) : '')),

            (new SelectField('locale'))
                ->options($localeOptions)
                ->defaultValue(config('app.fallback_locale'))
                ->label(Lang::get('fields.locale'))
                ->inputHidden(true)
                ->required(true),

            (new SelectField('role'))
                ->options($roleOptions)
                ->defaultValue('admin')
                ->label(Lang::get('fields.role'))
                ->inputHidden(true)
                ->required(true)
                ->fillable(false),

            (new BoolField('disabled_at'))
                ->stylingFormat('toggle')
                ->label(Lang::get('fields.active'))
                ->setValidation(['date'])
                ->defaultValue(null)
        ]);
    }

    /**
     * @inheritDoc
     */
    public function allowedApiRelations(): array
    {
        return [
            'roles'
        ];
    }

    public function allowRelationChanges($relationName = null): bool
    {
        return ($relationName === 'roles');
    }

    public function allowedApiAppends(): array
    {
        return [
            'friend_status',
            'avatar'
        ];
    }

    public function setDisabledAtAttribute($value): void
    {
        if ($value && !$this->disabled_at) {
            $this->attributes['disabled_at'] = Carbon::now();
        } elseif (!$value) {
            $this->attributes['disabled_at'] = null;
        }
    }

    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function preferredLocale(): ?string
    {
        return ($this->locale) ?: null;
    }

    public function stripeName(): string
    {
        return $this->full_name;
    }

    /**
     * The channel the user receives notification broadcasts on
     */
    public function receivesBroadcastNotificationsOn(): string
    {
        return 'users.' . $this->id;
    }

    public function gameIdentifiers(): HasMany
    {
        return $this->hasMany(GameIdentifier::class);
    }

    /**
     * Scope query based on ability
     *
     * @param Builder $query
     * @param string|array $ability
     * @param null|int $scope Optionally provide other Bouncer scope than current one
     *
     * @return Builder
     */
    public function scopeWhereCan(Builder $query, $ability, $scope = null): Builder
    {
        return $query->where(static function (Builder $query) use ($ability, $scope) {
            // Direct
            $query->whereHas('abilities', static function (Builder $query) use ($ability, $scope) {
                if (is_array($ability)) {
                    $query->whereIn('abilities.name', $ability);
                } else {
                    $query->where('abilities.name', $ability);
                }
                $query->where('abilities.scope', $scope);
            });
            // Through roles
            $query->orWhereHas('roles', static function (Builder $query) use ($ability, $scope) {
                $query->where('roles.scope', $scope);

                $query->whereHas('abilities', static function (Builder $query) use ($ability) {
                    if (is_array($ability)) {
                        $query->whereIn('abilities.name', $ability);
                    } else {
                        $query->where('abilities.name', $ability);
                    }
                });
            });
        });
    }

    public function scopeWhereActive(Builder $query): Builder
    {
        return $query->whereNull('disabled_at');
    }

    /**
     * Used only for currently logged in user. Helper method for keeping the eager laoded relations consistent
     */
    public function loadMyData(): static
    {
        return $this->loadMissing([
            'roles',
        ])->append(['avatar']);
    }

    public function toArray(): array
    {
        return parent::toArray();
    }
}
