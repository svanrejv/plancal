<?php

namespace App\Models;

use Bjerke\Bread\Builder\DefinitionBuilder;
use Bjerke\Bread\Fields\HasOneField;
use Bjerke\Bread\Fields\IntField;
use Bjerke\Bread\Fields\TextField;
use Bjerke\Bread\Models\BreadModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Lang;

class Team extends BreadModel
{
    protected function define(DefinitionBuilder $definition): DefinitionBuilder
    {
        return $definition->addFields([
            (new TextField('name'))
                ->label(Lang::get('fields.title'))
                ->required(true)
                ->minLength(3)
                ->maxLength(45),

            (new TextField('home_color'))
                ->label(Lang::get('fields.home_color'))
                ->minLength(3)
                ->maxLength(45),

            (new TextField('guest_color'))
                ->label(Lang::get('fields.guest_color'))
                ->minLength(3)
                ->maxLength(45),

            (new HasOneField())
                ->relation('league', $this)
                ->label(Lang::get('fields.league'))
                ->required(true),

            (new TextField('district'))
                ->label(Lang::get('fields.district'))
                ->minLength(3)
                ->maxLength(45),

            (new TextField('class'))
                ->label(Lang::get('fields.class'))
                ->minLength(3)
                ->maxLength(45),

            (new HasOneField())
                ->relation('venue', $this)
                ->label(Lang::get('fields.venue'))
                ->required(true),

            (new IntField('ext_id'))
                ->label(Lang::get('fields.external_identifier'))
                ->min(0),
        ]);
    }

    public function league(): BelongsTo
    {
        return $this->belongsTo(League::class);
    }

    public function venue(): HasOne
    {
        return $this->hasOne(Venue::class);
    }

}
