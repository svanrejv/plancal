<?php

namespace App\Models;

use Bjerke\Bread\Builder\DefinitionBuilder;
use Bjerke\Bread\Fields\DateTimeField;
use Bjerke\Bread\Fields\HasOneField;
use Bjerke\Bread\Fields\IntField;
use Bjerke\Bread\Fields\TextField;
use Bjerke\Bread\Models\BreadModel;
use Illuminate\Support\Facades\Lang;

class Game extends BreadModel
{
    protected function define(DefinitionBuilder $definition): DefinitionBuilder
    {
        return $definition->addFields([
            (new IntField('ext_id'))
                ->label(Lang::get('fields.external_identifier'))
                ->min(0),

            (new HasOneField('home_team_id'))
                ->relation('team', $this)
                ->label(Lang::get('models.game.home_team'))
                ->required(true),

            (new HasOneField('guest_team_id'))
                ->relation('team', $this)
                ->label(Lang::get('models.game.guest_team'))
                ->required(true),

            (new HasOneField())
                ->relation('venue', $this)
                ->label(Lang::get('models.game.venue'))
                ->required(false),

            (new HasOneField())
                ->relation('league', $this)
                ->label(Lang::get('models.game.league'))
                ->required(false),

            (new TextField('referee1'))
                ->label(Lang::get('models.game.referee1'))
                ->minLength(3),

            (new TextField('referee2'))
                ->label(Lang::get('models.game.referee2'))
                ->minLength(3),

            (new TextField('official1'))
                ->label(Lang::get('models.game.official1'))
                ->minLength(3),

            (new TextField('official2'))
                ->label(Lang::get('models.game.official2'))
                ->minLength(3),

            (new TextField('delegate'))
                ->label(Lang::get('models.game.delegate'))
                ->minLength(3),

            (new DateTimeField('start_at'))
                ->label(Lang::get('models.game.start_at'))


        ]);
    }
}
