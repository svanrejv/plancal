<?php

namespace App\Models;

use Bjerke\Bread\Builder\DefinitionBuilder;
use Bjerke\Bread\Fields\IntField;
use Bjerke\Bread\Fields\TextField;
use Bjerke\Bread\Models\BreadModel;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Lang;

class League extends BreadModel
{
    protected function define(DefinitionBuilder $definition): DefinitionBuilder
    {
        return $definition->addFields([
            (new TextField('name'))
                ->label(Lang::get('fields.title'))
                ->required(true)
                ->minLength(3)
                ->maxLength(45),

            (new TextField('district'))
                ->label(Lang::get('fields.district'))
                ->minLength(3)
                ->maxLength(45)
                ->required(true),

            (new IntField('period_runtime'))
                ->label(Lang::get('fields.period_runtime'))
                ->required(false),

            (new IntField('half_time'))
                ->label(Lang::get('fields.half_time'))
                ->required(false),

            (new IntField('over_time'))
                ->label(Lang::get('fields.over_time'))
                ->required(false)

        ]);
    }

    public function teams(): HasMany
    {
        return $this->hasMany(Team::class);
    }

}
