<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

/**
 * @mixin IdeHelperRole
 */
class Role extends \Silber\Bouncer\Database\Role
{
    /**
     * Scope query based on ability
     *
     * @param Builder $query
     * @param string|array $ability
     * @param null|int $scope Optionally provide other Bouncer scope than current one
     *
     * @return Builder
     */
    public function scopeWhereCan(Builder $query, $ability, $scope = null): Builder
    {
        return $query->whereHas('abilities', static function (Builder $query) use ($ability, $scope) {
            if (is_array($ability)) {
                $query->whereIn('abilities.name', $ability);
            } else {
                $query->where('abilities.name', $ability);
            }
            $query->where('abilities.scope', $scope);
        });
    }
}
