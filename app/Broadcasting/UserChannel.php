<?php

namespace App\Broadcasting;

use App\Models\User;

class UserChannel
{
    public function join(User $authedUser, User $requestedUser)
    {
        return $authedUser->id === $requestedUser->id;
    }
}
