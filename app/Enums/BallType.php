<?php

namespace App\Enums;

use Bjerke\Enum\BaseEnum;

final class BallType extends BaseEnum
{
    public const REGULAR = 10;
    public const PRACTICE = 20;
    public const FOAM = 30;
}
