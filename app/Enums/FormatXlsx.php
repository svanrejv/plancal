<?php

namespace App\Enums;

use Bjerke\Enum\BaseEnum;

final class FormatXlsx extends BaseEnum
{
    public const FORMAT_XLSX_SVHF_VENUE = 10;
}
