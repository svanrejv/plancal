<?php

namespace App\Channels;

use App\Contracts\UsesPushNotifications;
// use App\Models\Device;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging;
use Kreait\Laravel\Firebase\Facades\Firebase;

class PushChannel
{
    /**
     * Send notification to user devices
     */
    public function send(User $notifiable, UsesPushNotifications $notification)
    {
        $message = $notification->toPush($notifiable);

        if (!config('firebase.projects.app.credentials.file')) {
            Log::error('Push notification not sent. Missing credentials file');
            return;
        }

        /* @var Collection $devices */
        $devices = (isset($notifiable->token)) ? collect([$notifiable]) : $notifiable->devices()->get();

        if (!isset($message['notification']) || $devices->isEmpty()) {
            return;
        }

        $notificationData = [
            'notification' => [
                'title' => $message['notification']['title'],
                'body' => $message['notification']['body']
            ],
            'android' => [
                'notification' => [
                    'sound' => 'default'
                ]
            ],
            'apns' => [
                'payload' => [
                    'aps' => [
                        'sound' => 'default',
                        'mutable-content' => 1,
                        'badge' => $notifiable->unreadNotifications()->count() ?: 1,
                    ]
                ]
            ]
        ];

        $extra = (isset($message['data']) && is_array($message['data'])) ? $message['data'] : [];
        $extra['notification_type'] = class_basename($notification);
        $notificationData['data'] = array_map(static function ($value) {
            return (string) $value;
        }, $extra);

        $sendReport = Firebase::messaging()->sendMulticast(
            Messaging\CloudMessage::fromArray($notificationData),
            $devices->pluck('token')->unique()->toArray()
        );

        if ($sendReport->hasFailures()) {
            Log::error('--> Push notification failed. User: ' . $notifiable->id);
            foreach ($sendReport->failures()->getItems() as $failure) {
                Log::error($failure->error()->getMessage());
                Log::info('Target: ' . $failure->target()->value());
            }

            // foreach ($sendReport->unknownTokens() as $unkownToken) {
            //     Log::info('Deleting unknown token: ' . $unkownToken);
            //     Device::where('token', $unkownToken)->delete();
            // }
            //
            // foreach ($sendReport->invalidTokens() as $invalidToken) {
            //     Log::info('Deleting invalid token: ' . $invalidToken);
            //     Device::where('token', $invalidToken)->delete();
            // }
        }
    }
}
