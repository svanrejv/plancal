<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\BallType;
use App\Helpers\Locale;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class ProtocolController extends Controller
{
    /**
     * @param string|null $lang
     * @return array
     */
    public function getEnums(?string $lang = null): array
    {
        $useLocale = (!$lang) ? App::getLocale() : Locale::getQualifiedLocale($lang);
        App::setLocale($useLocale);

        if (!App::isLocal() && Cache::tags('protocol')->has(App::getLocale())) {
            $protocol = Cache::tags('protocol')->get(App::getLocale());
        } else {
            $protocol = [
                'ball_type' => [
                    'values' => BallType::getConstants(),
                    'i18n' => BallType::getTranslated()
                ],
            ];

            Cache::tags('protocol')->forever(App::getLocale(), $protocol);
        }

        return $protocol;
    }

    public function getRegions(): array
    {
        return config('app.regions');
    }

    public function getApiVersion(): array
    {
        return [
            'api' => config('app.version')
        ];
    }
}
