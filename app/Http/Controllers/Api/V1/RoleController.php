<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\Pagination;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Bjerke\ApiQueryBuilder\QueryBuilder;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $queryBuilder = new QueryBuilder(new Role(), $request);

        $query = $queryBuilder->build();

        if ($ability = $request->get('whereCan')) {
            $query->whereCan($ability);
        }

        return Pagination::addPagination($request, $query);
    }
}
