<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\Pagination;
use App\Models\User;
use App\Notifications\FriendRequestNotification;
use Bjerke\ApiQueryBuilder\Helpers\ColumnNameSanitizer;
use Bouncer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Bjerke\Bread\Helpers\RequestParams;
use Bjerke\Bread\Http\Controllers\BreadController;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Kreait\Laravel\Firebase\Facades\Firebase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends BreadController
{
    public function search(Request $request)
    {
        $searchString = $request->get('query');
        (Validator::make([
            'query' => $searchString
        ], [
            'query' => 'required|string|min:3'
        ]))->validate();

        return User::whereActive()
                   ->where('display_name', 'like', $searchString . '%')
                   ->where('id', '!=', Auth::id())
                   ->orderBy('display_name')
                   ->limit(50)
                   ->get();
    }

    public function create(Request $request, $with = [], $manualAttributes = [], $beforeSave = null)
    {
        $params = RequestParams::getParams($request);

        (Validator::make($params, [
            'password' => 'required|min:6'
        ]))->validate();

        $user = new User();
        $user->compileDefinition();
        $fillables = $user->getFillable();

        $data = array_filter($params, static fn ($field) => in_array($field, $fillables, true), ARRAY_FILTER_USE_KEY);

        if (
            !isset($data['locale']) ||
            !array_key_exists($data['locale'], config('app.supported_locales'))
        ) {
            $data['locale'] = (array_key_exists(App::getLocale(), config('app.supported_locales')))
                ? App::getLocale()
                : App::getFallbackLocale();
        }

        $user->fill($data);
        $user->validate();

        $auth = Firebase::auth();
        $fbUser = $auth->createUser([
            'email' => $params['email'],
            'password' => $params['password'],
            'disabled' => false
        ]);

        $user->uuid = $fbUser->uid;
        $user->save();

        if ($user instanceof User) {
            $auth->sendEmailVerificationLink($user->email, [
                // 'continueUrl' => config('app.welcome_url')
            ], $user->locale);

            $role = (isset($params['role']) &&
                     Bouncer::role()->whereName($params['role'])->exists()
            ) ? $params['role'] : 'user';

            // If user is not allowed to create selected role, fallback to lowest possible role
            if (!Auth::user() || !Auth::user()->can('create-' . $role)) {
                $role = 'user';
            }

            Bouncer::assign($role)->to($user);
            Bouncer::refreshFor($user);
        }

        return $user;
    }

    public function update(Request $request, $id, $with = [], $applyQuery = null, $beforeSave = null)
    {
        $params = RequestParams::getParams($request);

        /** @var bool|User $user */
        $user = parent::update($request, $id, $with, $applyQuery, $beforeSave);

        if (
            isset($params['role']) &&
            $user instanceof User &&
            !$user->isA($params['role']) &&
            Bouncer::role()->whereName($params['role'])->exists() &&
            Auth::user()->can('create-' . $params['role'])
        ) {
            Bouncer::sync($user)->roles($params['role']);
            Bouncer::refreshFor($user);
        }

        return $user;
    }

    /**
     * Updates currently logged in user
     */
    public function updateMe(Request $request)
    {
        $user = Auth::user();
        if (!$user instanceof User) {
            throw new HttpException(404, 'User not found');
        }

        $updatedUser = $this->update($request, $user->id);
        assert($updatedUser instanceof User);

        return $updatedUser->loadMyData();
    }

    public function deleteMe(Request $request): ?bool
    {
        $user = Auth::user();
        if (!$user instanceof User) {
            throw new HttpException(404, 'User not found');
        }

        return $user->deleteOrFail();
    }

    /**
     * Get the authenticated User.
     *
     * @return User|\Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function me()
    {
        return Auth::user()->loadMyData();
    }

    /**
     * Returns abilities for currently logged in user
     */
    public function myAbilities(Request $request)
    {
        return Auth::user()->getAbilities();
    }

    /**
     * Returns role for currently logged in user
     */
    public function myRole(Request $request)
    {
        return Auth::user()->getRoles();
    }

    /**
     * List notifications, defaults to only query unread
     */
    public function myNotifications(Request $request)
    {
        $fromAllNotifications = $request->get('read', false);
        if (
            $fromAllNotifications === true ||
            $fromAllNotifications === 'true'
        ) {
            $query = Auth::user()?->notifications();
        } else {
            $query = Auth::user()?->unreadNotifications();
        }

        $query->latest();

        if (($where = $request->get('where')) !== null) {
            $allowedQueryColumns = ['type'];

            foreach ($where as $column => $value) {
                if (!in_array($column, $allowedQueryColumns, true)) {
                    continue;
                }

                if (is_array($value)) {
                    if (isset($value['operator'], $value['value'])) {
                        $query->where($column, $value['operator'], $value['value']);
                    }
                } else {
                    $query->where($column, $value);
                }
            }
        }

        if ($request->get('only_read', false)) {
            $query->whereNotNull('read_at');
        }

        if ($request->get('only_count') !== null) {
            return $query->count();
        }
        if (
            ($limit = $request->get('limit')) !== null &&
            is_numeric($limit) &&
            $limit > 0
        ) {
            $query->limit($limit);
        }

        if (
            ($pagination = $request->get('paginate')) !== null &&
            ($pagination === false || $pagination === 'false' || $pagination === '0')
        ) {
            return $query->get();
        }

        $perPage = $request->get('per_page');

        $originalPaginator = $query->paginate($perPage)->appends($request->query());
        $senderIds = collect($originalPaginator->items())->pluck('data.sender_uuid')->unique()->filter();
        $senders = collect([]);
        if ($senderIds->isNotEmpty()) {
            User::mergeAppends(['avatar']);
            $senders = User::with('media')->whereIn('uuid', $senderIds)->get();
        }

        return tap($originalPaginator, static function ($paginator) use ($senders) {
            return $paginator->getCollection()->transform(function (DatabaseNotification $notification) use ($senders) {
                if (isset($notification->data['sender_uuid'])) {
                    $data = $notification->data;
                    $data['sender'] = $senders->firstWhere('uuid', $notification->data['sender_uuid']);
                    $notification->data = $data;
                }
                return $notification;
            });
        });
    }

    /**
     * Mark a single unread notification as read
     */
    public function markNotificationRead(
        Request $request,
        string|int $id
    ): DatabaseNotification|DatabaseNotificationCollection {
        if ($id !== 'all') {
            $notification = Auth::user()?->unreadNotifications()->where('id', $id)->first();

            if (!$notification) {
                throw (new ModelNotFoundException())->setModel(
                    DatabaseNotification::class,
                    $id
                );
            }

            assert($notification instanceof DatabaseNotification);
            $notification->markAsRead();
            return $notification;
        }

        $notifications = Auth::user()?->unreadNotifications()->get();
        assert($notifications instanceof DatabaseNotificationCollection);
        $notifications->markAsRead();

        return $notifications;
    }
}
