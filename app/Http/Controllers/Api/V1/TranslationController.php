<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AdapterInterface;

class TranslationController extends Controller
{
    /**
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * Returns all translations for specified language + fallback
     *
     * @param Request $request
     * @param string  $lang
     *
     * @return array
     */
    public function index(Request $request, string $lang): array
    {
        $translations = [];

        if (Cache::tags('translations')->has($lang) || $this->files()->has($lang)) {
            $translations[$lang] = $this->compileLanguage($lang);
        }

        $fallback = $request->get('fallback', Lang::getFallback());

        if ($fallback !== $lang) {
            $translations[$fallback] = $this->compileLanguage($fallback);
        }

        return $translations;
    }

    /**
     * Returns current translation version
     *
     * @param Request $request
     *
     * @return array
     */
    public function getVersion(Request $request)
    {
        return [
            'version' => config('app.translation_version')
        ];
    }

    /**
     * Returns array of supported language objects
     *
     * @param Request $request
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getSupportedLanguages(Request $request)
    {
        return config('app.supported_locales');
    }

    /**
     * Compiles all available translation strings from given language
     *
     * @param string $lang
     *
     * @return array
     */
    private function compileLanguage(string $lang): array
    {
        if (Cache::tags('translations')->has($lang)) {
            return Cache::tags('translations')->get($lang);
        }

        $strings = [];
        $prefix = $this->files()->getPathPrefix();
        foreach ($this->files()->listContents($lang) as $dir) {
            if (Str::contains($dir['path'], 'php')) {
                $key = str_replace(["$lang/", '.php'], '', $dir['path']);
                $strings[$key] = include($prefix . $dir['path']);
            }
        }

        Cache::tags('translations')->forever($lang, $strings);

        return $strings;
    }

    private function files()
    {
        if ($this->adapter === null) {
            $this->adapter = new Local(base_path('resources/lang'));
        }

        return $this->adapter;
    }
}
