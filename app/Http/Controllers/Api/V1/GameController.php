<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\FormatXlsx;
use Bjerke\Bread\Helpers\RequestParams;
use Bjerke\Bread\Http\Controllers\BreadController;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GameController extends BreadController
{
    public function view(Request $request, $id, ?Closure $applyQuery = null): Model
    {
        return parent::view($request, $id, $applyQuery);
    }

    public function index(Request $request, ?Closure $applyQuery = null)
    {
        return parent::index($request, function (Builder $query) use ($request) {

        });
    }

    public function importXlsx($request){
        $params = RequestParams::getParams($request);

        (Validator::make($params, [
            'format' => 'required|in:' . implode(',', array_values(FormatXlsx::getConstants())),
            'file' => 'required|base64'
        ]))->validate();


    }
}
