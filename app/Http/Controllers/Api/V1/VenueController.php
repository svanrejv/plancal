<?php

namespace App\Http\Controllers\Api\V1;

use Bjerke\Bread\Http\Controllers\BreadController;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Closure;
use Illuminate\Http\Request;

class VenueController extends BreadController
{
    public function view(Request $request, $id, ?Closure $applyQuery = null): Model
    {
        return parent::view($request, $id, $applyQuery);
    }

    public function index(Request $request, ?Closure $applyQuery = null)
    {
        return parent::index($request, function (Builder $query) use ($request) {

        });
    }
}
