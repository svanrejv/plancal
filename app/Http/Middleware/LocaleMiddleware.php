<?php

namespace App\Http\Middleware;

use App\Helpers\Locale;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

/**
 * Class LocaleMiddleware
 * @package App\Http\Middleware
 *
 * Hooks in before the request is handled by the application and sets locale based on header
 */
class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (($user = Auth::user()) && $user->locale) {
            App::setLocale($user->locale);
        } elseif (($locale = $request->header('Accept-Language')) && is_string($locale)) {
            App::setLocale(Locale::getQualifiedLocale($locale));
        }

        return $next($request);
    }
}
