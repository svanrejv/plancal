<?php

namespace App\Http\Middleware;

use App\Helpers\JWT;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Closure;

class Authenticate
{
    public const KEY_TYPES = [
        'client' => 'API_KEY_CLIENT'
    ];

    public function handle($request, Closure $next, string $type, $requirement = null)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return $next($request);
        }

        switch ($type) {
            case 'key':
                //Validate API key
                $keyTypes = explode('|', $requirement);
                $authenticated = $this->authenticateKey($request, $keyTypes);
                break;
            default:
                $authenticated = $this->authenticateUser($request);
        }

        if (!$authenticated) {
            throw new HttpException(403, 'Insufficient permissions');
        }

        return $next($request);
    }

    /**
     * Validates API key
     *
     * @param \Illuminate\Http\Request $request
     * @param array|string $keyTypes Array of allowed key types or string of single type. Provide 'any' to allow all
     *
     * @return bool
     */
    private function authenticateKey(Request $request, $keyTypes): bool
    {
        $providedKey = JWT::getValue($request, 'Apikey');

        if (is_string($keyTypes)) {
            $keyTypes = [$keyTypes];
        }

        if ($keyTypes[0] === 'any') {
            $keyTypes = array_keys(self::KEY_TYPES);
        }

        $authenticated = false;

        foreach ($keyTypes as $type) {
            if ($providedKey === config('app.' . Str::lower(self::KEY_TYPES[$type]))) {
                $authenticated = true;
                break;
            }
        }

        return $authenticated;
    }

    /**
     * Validates user is logged in
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     *
     * @throws \Kreait\Firebase\Exception\AuthException
     * @throws \Kreait\Firebase\Exception\FirebaseException
     */
    private function authenticateUser(Request $request): bool
    {
        $user = JWT::getAuthenticatedUser($request);
        return ($user instanceof User);
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request): ?string
    {
        if (! $request->expectsJson()) {
            return route('login');
        }

        return null;
    }
}
