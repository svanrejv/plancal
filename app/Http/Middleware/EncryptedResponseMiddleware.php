<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Decrypts request and encrupts response
 */
class EncryptedResponseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return $next($request);
        }

        $decryptedBody = json_decode(
            Crypt::decrypt($request->json('encrypted_data')),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        $request->setJson(new ParameterBag($decryptedBody));

        /** @var Response $response */
        $response = $next($request);

        $response->setContent(
            json_encode([
                'encrypted_data' => Crypt::encrypt($response->getContent())
            ], JSON_THROW_ON_ERROR)
        );

        return $response;
    }
}
