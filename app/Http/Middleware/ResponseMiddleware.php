<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Validation\ValidationException;

/**
 * Class ResponseMiddleware
 * @package App\Http\Middleware
 *
 * Hooks in before a response is sent to the client and unifies the response structure
 */
class ResponseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            $request->getMethod() === 'OPTIONS' ||
            !$request->wantsJson()
        ) {
            return $next($request);
        }

        $success = true;
        $data = [];
        $error = '';
        $errorInfo = [
            'status_code' => 200
        ];

        /** @var Response $response */
        $response = $next($request);

        if ($response->exception !== null) {
            $success = false;

            if ($request->header('debug') && config('app.env') === 'local') {
               // if(method_exists($response->exception, 'getTrace')) {
               //     $error_info['trace'] = $response->exception->getTraceAsString();
               // }

                if (property_exists($response->exception, 'xdebug_message')) {
                    $errorInfo['xdebug_message'] = strip_tags($response->exception->xdebug_message);
                }
            }

            $error = $response->exception->getMessage();
            $errorInfo['status_code'] = $response->getStatusCode();

            if ($response->exception instanceof ValidationException) {
                //ValidationException has been thrown, provide errors
                $error = Lang::get('validation.failed');
                $errorInfo['validation_errors'] = $response->exception->errors();
                $errorInfo['status_code'] = 422;
            }

            if (!$error) {
                $error = Response::$statusTexts[$response->getStatusCode()];
            }
        } else {
            $errorInfo = [
                'status_code' => $response->getStatusCode()
            ];

            if ($response->getStatusCode() === 422) {
                //Validation error
                $success = false;
                $error = Lang::get('validation.failed');
                $errorInfo['validation_errors'] = $response->original;
            } else {
                $data = $response->original;
            }
        }

        $headers = $response->headers->all();
        $headers['Content-Timezone'] = config('app.timezone');
        $headers['Content-Language'] = App::getLocale();
        $headers['Content-Type'] = 'application/json';

        return response()->json(
            [
                'success' => $success,
                'data' => $data,
                'error' => $error,
                'error_info' => $errorInfo
            ],
            $errorInfo['status_code'],
            $headers
        );
    }
}
