<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class CacheControlMiddleware
 * @package App\Http\Middleware
 *
 * Applies extra cache control directives to ensure cache is kept updated
 */
class CacheControlMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $append
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $append = 'must-revalidate, proxy-revalidate')
    {
        $response = $next($request);

        $headers = $response->headers->all();

        $cacheHeaders = [];
        if ($append) {
            $cacheHeaders = array_map('trim', explode(',', $append));
        }

        if (isset($headers['Cache-Control'])) {
            $existingCacheHeaders = array_map('trim', explode(',', $headers['Cache-Control']));
            $cacheHeaders = array_merge($cacheHeaders, $existingCacheHeaders);
        }

        $response->header('Cache-Control', implode(', ', $cacheHeaders));

        return $response;
    }
}
