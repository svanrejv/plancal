<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserUpdated implements ShouldBroadcast
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public function __construct(
        public User $user
    ) {
    }

    public function broadcastAs(): string
    {
        return 'user.updated';
    }

    public function broadcastOn(): Channel
    {
        return new PrivateChannel('users.' . $this->user->id);
    }

    public function broadcastWith(): array
    {
        $this->user->allowSensitive = true;
        return [
            'user' => $this->user->loadMyData()
        ];
    }
}
