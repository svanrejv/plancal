<?php

namespace App\Observers;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Models\User;
use Bouncer;
use Kreait\Laravel\Firebase\Facades\Firebase;

class UserObserver
{
    public function creating(User $user): void
    {

    }

    public function saving(User $user): void
    {

    }

    public function created(User $user): void
    {
        UserCreated::dispatch($user);
    }

    public function updated(User $user): void
    {
        if ($user->disabled_at) {

        }

        Bouncer::refreshFor($user);
        UserUpdated::dispatch($user);
    }

    public function deleting(User $user)
    {
        Bouncer::sync($user)->roles([]);
        Bouncer::sync($user)->abilities([]);
        Bouncer::refreshFor($user);
    }

    public function deleted(User $user): void
    {
        Firebase::auth()->deleteUser($user->uuid);
        $user->notifications()->delete();
    }
}
