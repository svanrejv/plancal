"use strict";
(self["webpackChunksimway_golf_api"] = self["webpackChunksimway_golf_api"] || []).push([["resources_shared_js_Components_User_Avatar_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js ***!
  \************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.includes.js */ "./node_modules/core-js/modules/es.array.includes.js");
/* harmony import */ var core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_includes_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _shared_js_Helpers_formatUserDisplayName__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/js/Helpers/formatUserDisplayName */ "./resources/shared/js/Helpers/formatUserDisplayName.js");


/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    user: Object,
    extraClass: {
      type: String,
      default: 'outline'
    },
    avatarFormat: {
      type: String,
      default: function _default() {
        return 'round';
      },
      validator: function validator(value) {
        return ['square', 'round'].includes(value);
      }
    }
  },
  methods: {
    showDisplayName: _shared_js_Helpers_formatUserDisplayName__WEBPACK_IMPORTED_MODULE_1__["default"],
    getAvatarURL: function getAvatarURL() {
      if (this.user && this.user.avatar && Array.isArray(this.user.avatar) && this.user.avatar.length > 0 && this.user.avatar[0].sizes) {
        return this.user.avatar[0].sizes.thumb;
      } else {
        return '/assets/images/avatar.svg';
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* binding */ render; }
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.runtime.esm-bundler.js");


var _withScopeId = function _withScopeId(n) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.pushScopeId)("data-v-8d8cfa50"), n = n(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.popScopeId)(), n;
};

var _hoisted_1 = ["alt", "src"];
var _hoisted_2 = ["alt", "src"];
function render(_ctx, _cache, $props, $setup, $data, $options) {
  return (0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("div", {
    class: (0,vue__WEBPACK_IMPORTED_MODULE_0__.normalizeClass)(["avatar", [$props.extraClass, $props.avatarFormat]])
  }, [$props.extraClass.indexOf('fancy-border') >= 0 ? ((0,vue__WEBPACK_IMPORTED_MODULE_0__.openBlock)(), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementBlock)("img", {
    key: 0,
    class: "border",
    alt: $options.showDisplayName($props.user),
    src: $options.getAvatarURL()
  }, null, 8
  /* PROPS */
  , _hoisted_1)) : (0,vue__WEBPACK_IMPORTED_MODULE_0__.createCommentVNode)("v-if", true), (0,vue__WEBPACK_IMPORTED_MODULE_0__.createElementVNode)("img", {
    class: "main",
    alt: $options.showDisplayName($props.user),
    src: $options.getAvatarURL()
  }, null, 8
  /* PROPS */
  , _hoisted_2)], 2
  /* CLASS */
  );
}

/***/ }),

/***/ "./resources/shared/js/Helpers/formatUserDisplayName.js":
/*!**************************************************************!*\
  !*** ./resources/shared/js/Helpers/formatUserDisplayName.js ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* export default binding */ __WEBPACK_DEFAULT_EXPORT__; }
/* harmony export */ });
/* harmony import */ var _i18n__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./i18n */ "./resources/shared/js/Helpers/i18n.js");

/* harmony default export */ function __WEBPACK_DEFAULT_EXPORT__(user) {
  if (user) {
    if (user.display_name) {
      return user.display_name;
    } else if (user.first_name) {
      return user.first_name;
    }
  } else {
    return _i18n__WEBPACK_IMPORTED_MODULE_0__["default"].get('users.removed_user');
  }
}

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, ".avatar[data-v-8d8cfa50] {\n  position: relative;\n  overflow: hidden;\n  display: block;\n  box-shadow: var(--box-shadow);\n}\n.avatar img[data-v-8d8cfa50] {\n  vertical-align: middle;\n  max-width: 100%;\n  height: calc(var(--top-bar-height) - var(--global-spacing-half) * 2);\n}\n.avatar .main[data-v-8d8cfa50] {\n  z-index: 3;\n  position: relative;\n}\n.avatar .border[data-v-8d8cfa50] {\n  z-index: 1;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\n.avatar.round[data-v-8d8cfa50] {\n  border-radius: 50%;\n}\n.avatar.round img[data-v-8d8cfa50] {\n  border-radius: 50%;\n}\n.avatar.rounded[data-v-8d8cfa50] {\n  border-radius: 32px;\n}\n.avatar.rounded .main[data-v-8d8cfa50] {\n  border-radius: 32px;\n}\n.avatar.square[data-v-8d8cfa50] {\n  flex-grow: 0;\n  flex-shrink: 2;\n  flex-basis: var(--image-size);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: var(--global-spacing-half);\n  border-radius: var(--global-radius-small);\n}\n.avatar.square .main[data-v-8d8cfa50] {\n  width: var(--image-size);\n  height: auto;\n  border-radius: var(--global-radius-small);\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.avatar.outline .main[data-v-8d8cfa50] {\n  border: 1px solid var(--color-medium-tint-700);\n}\n.avatar.fancy-border[data-v-8d8cfa50] {\n  padding: 6px;\n  box-shadow: none;\n}\n.avatar.fancy-border img[data-v-8d8cfa50] {\n  width: 100%;\n  max-width: none;\n  height: auto;\n}\n.avatar.fancy-border .main[data-v-8d8cfa50] {\n  border-radius: 26px;\n}\n.avatar.fancy-border[data-v-8d8cfa50]:after {\n  border-radius: 32px;\n  overflow: hidden;\n  content: \"\";\n  z-index: 2;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  -webkit-backdrop-filter: blur(10px);\n          backdrop-filter: blur(10px);\n  background: linear-gradient(0deg, rgba(214, 184, 94, 0), rgba(214, 184, 94, 0)), radial-gradient(100% 100% at 100% 100%, rgba(180, 143, 58, 0.6) 0%, rgba(180, 143, 58, 0) 100%), radial-gradient(100% 100% at 0% 0%, rgba(255, 251, 164, 0.6) 0%, rgba(255, 251, 164, 0) 100%);\n}", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_style_index_0_id_8d8cfa50_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_style_index_0_id_8d8cfa50_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ __webpack_exports__["default"] = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_style_index_0_id_8d8cfa50_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./resources/shared/js/Components/User/Avatar.vue":
/*!********************************************************!*\
  !*** ./resources/shared/js/Components/User/Avatar.vue ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Avatar_vue_vue_type_template_id_8d8cfa50_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true */ "./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true");
/* harmony import */ var _Avatar_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Avatar.vue?vue&type=script&lang=js */ "./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js");
/* harmony import */ var _Avatar_vue_vue_type_style_index_0_id_8d8cfa50_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss */ "./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss");
/* harmony import */ var _Users_christoffermossberg_Utveckling_webb_plancal_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./node_modules/vue-loader/dist/exportHelper.js */ "./node_modules/vue-loader/dist/exportHelper.js");




;


const __exports__ = /*#__PURE__*/(0,_Users_christoffermossberg_Utveckling_webb_plancal_node_modules_vue_loader_dist_exportHelper_js__WEBPACK_IMPORTED_MODULE_3__["default"])(_Avatar_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__["default"], [['render',_Avatar_vue_vue_type_template_id_8d8cfa50_scoped_true__WEBPACK_IMPORTED_MODULE_0__.render],['__scopeId',"data-v-8d8cfa50"],['__file',"resources/shared/js/Components/User/Avatar.vue"]])
/* hot reload */
if (false) {}


/* harmony default export */ __webpack_exports__["default"] = (__exports__);

/***/ }),

/***/ "./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js":
/*!********************************************************************************!*\
  !*** ./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js ***!
  \********************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__["default"]; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Avatar.vue?vue&type=script&lang=js */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=script&lang=js");
 

/***/ }),

/***/ "./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true":
/*!**************************************************************************************************!*\
  !*** ./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true ***!
  \**************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": function() { return /* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_template_id_8d8cfa50_scoped_true__WEBPACK_IMPORTED_MODULE_0__.render; }
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_dist_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_template_id_8d8cfa50_scoped_true__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../../../node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/dist/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=template&id=8d8cfa50&scoped=true");


/***/ }),

/***/ "./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss":
/*!*****************************************************************************************************************!*\
  !*** ./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss ***!
  \*****************************************************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_use_1_node_modules_vue_loader_dist_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_use_3_node_modules_vue_loader_dist_index_js_ruleSet_0_use_0_Avatar_vue_vue_type_style_index_0_id_8d8cfa50_scoped_true_lang_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!../../../../../node_modules/vue-loader/dist/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!../../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!../../../../../node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-12.use[1]!./node_modules/vue-loader/dist/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12.use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12.use[3]!./node_modules/vue-loader/dist/index.js??ruleSet[0].use[0]!./resources/shared/js/Components/User/Avatar.vue?vue&type=style&index=0&id=8d8cfa50&scoped=true&lang=scss");


/***/ })

}]);