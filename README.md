Download the repository. Remember to disconnect git if you're cloning.

Configure your .env file.

Make any necessary changes to version or packages in composer.json and package.json

In Homestead:
- Run `composer install`
- Run `artisan key:generate`
- Run `artisan migrate`
- Run `artisan storage:link`
- Run `artisan abilities:generate` - Sync the configuration from config/abilities.php to database
- Run `artisan translations:generate` - Compile current translations to json format accessible by frontend
- Run `artisan users:generate` - To generate a new user

To see available artisan commands run `artisan` and it will display all registered commands.

Outside Homestead:
- Run `npm install`

Start compiler in development mode (outside Homestead):
- Run `npm run hot` for hot module reload
- Run `npm run watch` for continuously compiling but without HMR
- Run `npm run dev` for compiling once

### Socket
Run websocket (in homestead): `artisan websockets:serve`

### Laravel documentation
https://laravel.com/docs/8.x/

Useful documentation links:
- API Query Builder: https://github.com/jesperbjerke/laravel-api-query-builder
- BREAD: https://github.com/jesperbjerke/laravel-bread
- Enums: https://github.com/jesperbjerke/laravel-enums

### Laravel best practices
https://github.com/alexeymezenin/laravel-best-practices

### Naming conventions in Laravel
https://github.com/alexeymezenin/laravel-best-practices#follow-laravel-naming-conventions

### Linting
- For php: `composer run lint` | `composer run lint-fix`
- For javascript: `npm run lint` | `npm run lint-fix`

### Configure automatic linting in PhpStorm
PHP Codesniffer and Larastan (PHPStan) as well as es-lint comes preinstalled and preconfigured. If you want PHPStorm to automatically show you linting errors (which you really should), you can easily enable this:
1. Go into preferences and check which PHP intepreter you're using (Languages & Frameworks / PHP). Make sure it's the version that the project should run in. Preferably you'll configure the interpreter to use the same as the Homestead instance.
2. Go into “Quality Tools” (Languages & Frameworks / PHP / Quality Tools), expand “PHP_CodeSniffer”, select “By default project interpreter”, and do the same for “PHPStan”.
3. Then go into Editor / Inspections. Expand “PHP”, and then “Quality tools”
4. Enable PHP_CodeSniffer validation (in the files to check, write “php,inc”), enable “Show sniff name”. Then under coding standard, select “Custom” and then the “…”-button, and select the “phpcs.xml” file that should be in the root of the project.
5. Enable PHPStan validation, Set “Level” to 5, select the configuration file “phpstan.neon” in the root of the project. Set memory limit to “2G”
6. Go to “Languages & Frameworks / Javascript / Code Quality Tools / Eslint. Select “Automatic ESLint configuration”.
7. Go to Editor / Inspections, Open “Javascript and Typescript”, expand “Code quality tools” and enable ESLint, and select “Use rule severity from configuration file”

## Development environment

Use Homestead. See more info here: https://gitlab.com/westartdev/snippets/homestead

### Recommended PhpStorm plugins / dev tools

#### Generic:
- .env files support (https://plugins.jetbrains.com/plugin/9525--env-files-support)
- .ignore (https://plugins.jetbrains.com/plugin/7495--ignore)
- Git Flow Integration (https://plugins.jetbrains.com/plugin/7315-git-flow-integration)
- SonarLint (https://plugins.jetbrains.com/plugin/7973-sonarlint)
- Php Inspections (EA Extended) (https://plugins.jetbrains.com/plugin/7622-php-inspections-ea-extended-)
- deep-assoc-completion (https://plugins.jetbrains.com/plugin/9927-deep-assoc-completion)

#### Laravel / Vue specific:
- IntelliVue (https://plugins.jetbrains.com/plugin/12014-intellivue)
- Laravel (https://plugins.jetbrains.com/plugin/7532-laravel)
- Laravel Tinker (https://plugins.jetbrains.com/plugin/14957-laravel-tinker)

#### Debugging tools:
Browser extensions:
- Clockwork (https://underground.works/clockwork/)
- Vue DevTools (https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en)

### Working with version control, merge requests, git flow and semantic versioning
https://gitlab.com/westartdev/policies-and-standards/-/wikis/policies/version-control

### Working with 3rd party libraries
https://gitlab.com/westartdev/policies-and-standards/-/wikis/policies/3rd-party-libraries

## CLI Commands

*IMPORTANT:* On Cloudnet servers. Make sure your are running commands as "www-data" (eg: `sudo -u www-data php artisan xxx`).

### Npm build only specific env
Argument to compile only part of the project `--onlyenvs=admin,web,game` one or many. With no args it runs everything. Example: `npm run hot --onlyenvs=admin`

### Generate a new Game Version
1. Upload the file to a place on the server where it's accessible for the application (like /var/www/html on staging). Make sure it's a zip file.
2. Go into the application root and run `php artisan game-versions:generate {path-to-zip-file}`
3. Type in the new version when the CLI asks for it

## Misc

### Initial URL

The initial URL is: https://web.plancal.local/login/

# How to play in browser / debug
Set localStorage variable browser_debug to true and you will be able to play without game being connected
