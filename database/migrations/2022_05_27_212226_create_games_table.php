<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->integer('ext_id')->nullable();
            $table->foreignId('home_team_id')->constrained('teams');
            $table->foreignId('guest_team_id')->constrained('teams');;
            $table->foreignId('venue_id')->nullable();
            $table->foreignId('league_id')->nullable();
            $table->string('referee1')->nullable();
            $table->string('referee2')->nullable();
            $table->string('official1')->nullable();
            $table->string('official2')->nullable();
            $table->string('delegate')->nullable();
            $table->dateTime('start_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
