<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Register roles and abilities
         */
        $this->remapRootAbilities();

        \Bouncer::refresh();
    }

    private function remapRootAbilities()
    {
        $roles = config('abilities.registry.roles');

        foreach ($roles as $roleName => $role) {
            \Bouncer::sync($roleName)->abilities(config('abilities.registry.roles.' . $roleName));
        }
    }
}
