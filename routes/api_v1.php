<?php

use App\Http\Controllers\Api\V1\BroadcastController;
use App\Http\Controllers\Api\V1\GameController;
use App\Http\Controllers\Api\V1\LeagueController;
use App\Http\Controllers\Api\V1\ProtocolController;
use App\Http\Controllers\Api\V1\RoleController;
use App\Http\Controllers\Api\V1\TeamController;
use App\Http\Controllers\Api\V1\TranslationController;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\VenueController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * Translations
 */
Route::group(['prefix' => 'translations'], static function () {
    Route::get('version', [TranslationController::class, 'getVersion']);
    Route::get('languages', [TranslationController::class, 'getSupportedLanguages']);
    Route::get('{lang}', [TranslationController::class, 'index'])->middleware('cache.headers:public;max_age=3600;etag');
});

/**
 * Protocol (Enums, configurations etc)
 */
Route::group(['prefix' => 'protocol'], static function () {
    Route::get('enums/{lang}', [ProtocolController::class, 'getEnums'])->middleware([
        'cache.headers:public;max_age=7200;etag',
        'cachecontrol'
    ]);

    Route::get('enums', [ProtocolController::class, 'getEnums']);
    Route::get('version', [ProtocolController::class, 'getApiVersion']);
});

Route::post('broadcasting/auth', [BroadcastController::class, 'authenticate'])->middleware('auth:user');
Route::get('broadcasting/auth', [BroadcastController::class, 'authenticate'])->middleware('auth:user');

/**
 * Users
 */
Route::group([
    'prefix' => 'users'
], static function () {
    Route::get('definition', [UserController::class, 'definition'])
        ->middleware('cache.headers:public;max_age=3600;etag');

    Route::post('', [UserController::class, 'create']);

    // TODO: implement this
    // Route::delete('devices/{token}', [UserController::class, 'unregisterDevice']);

    Route::group([
        'prefix' => 'me'
    ], static function () {
        Route::group([
            'middleware' => [
                'auth:user'
            ]
        ], static function () {
            Route::get('notifications', [UserController::class, 'myNotifications']);
            Route::patch('notifications/{id}/read', [UserController::class, 'markNotificationRead']);

            Route::get('abilities', [UserController::class, 'myAbilities']);
            Route::get('role', [UserController::class, 'myRole']);

            Route::get('', [UserController::class, 'me']);
            Route::patch('', [UserController::class, 'updateMe']);
            Route::delete('', [UserController::class, 'deleteMe']);
        });
    });
});

/**
 * Roles
 */
Route::group([
    'prefix' => 'roles',
    'middleware' => [
        'auth:user'
    ]
], static function () {
    Route::get('', [RoleController::class, 'index'])->middleware('can:view-roles');
});

/**
 * Venues
 */
Route::group([
    'prefix' => 'venues',
    'middleware' => [
        'auth:user'
    ]
], static function () {
    Route::get('definition', [VenueController::class, 'definition']);

    Route::get('', [VenueController::class, 'index']);;
    Route::post('', [VenueController::class, 'create']);;
});

/**
 * Games
 */
Route::group([
    'prefix' => 'venues',
    'middleware' => [
        'auth:user'
    ]
], static function () {
    Route::get('definition', [GameController::class, 'definition']);

    Route::get('', [GameController::class, 'index']);;
    Route::post('', [GameController::class, 'create']);;
});

/**
 * Teams
 */
Route::group([
    'prefix' => 'teams',
    'middleware' => [
        'auth:user'
    ]
], static function () {
    Route::get('definition', [TeamController::class, 'definition']);

    Route::get('', [TeamController::class, 'index']);;
    Route::post('', [TeamController::class, 'create']);;
});

/**
 * Leagues
 */
Route::group([
    'prefix' => 'leagues',
    'middleware' => [
        'auth:user'
    ]
], static function () {
    Route::get('definition', [LeagueController::class, 'definition']);

    Route::get('', [LeagueController::class, 'index']);;
    Route::post('', [LeagueController::class, 'create']);;
});
