<?php

/*
|--------------------------------------------------------------------------
| Holds default settings for clubs
|--------------------------------------------------------------------------
*/

return [
    'creation_cost_product' => env('CLUB_CREATION_COST_PRODUCT'),
    'member_fee_transferal_percentage' => 0.05
];
