<?php

/*
|-----------------------------------------------------------
| Hold configuration values related to handicap calculations
|-----------------------------------------------------------
*/

return [
    'base_hcp' => '40.4'
];
