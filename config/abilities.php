<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Holds possible abilities in relation to their specific role or context
    |--------------------------------------------------------------------------
    |
    | Used in BouncerSeeder to generate proper relation between role and abilities,
    | as well as specific model roles.
    |
    */
    'registry' => [

        /*
        |--------------------------------------------------------------------------
        | Roles related to their generic role
        |--------------------------------------------------------------------------
        |
        | Used in BouncerSeeder to generate proper relation between role and abilities
        |
        */
        'roles' => [
            'admin' => [
                'access-backoffice',
                'view-users',
                'manage-users',
                'view-roles',

                'create-user',
                'create-admin',
            ],

            'user' => [

            ]
        ]
    ],

];
