<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Base currency
    |--------------------------------------------------------------------------
    |
    | This is the base currency configured for your application
    |
    */

    'currency' => env('CASHIER_CURRENCY', 'usd'),

    'success_url' => env('CHECKOUT_SUCCESS_URL'),
    'cancel_url' => env('CHECKOUT_CANCEL_URL')

];
