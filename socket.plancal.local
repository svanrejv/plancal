server {
    #listen 80;
    listen 443 ssl http2;
    server_name .socket.plancal.local;
    root "/var/www/plancal/public";

    index index.html index.htm index.php;

    charset utf-8;

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/socket.plancal.local-error.log error;

    sendfile off;

    location / {
        proxy_pass                          http://127.0.0.1:6001;
        proxy_set_header Host               $host;
        proxy_set_header X-Real-IP          $remote_addr;

        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  https;
        proxy_set_header X-VerifiedViaNginx yes;
        proxy_read_timeout                  60;
        proxy_connect_timeout               60;
        proxy_redirect                      off;

        # Specific for websockets: force the use of HTTP/1.1 and set the Upgrade header
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }

    location ~ /\.ht {
        deny all;
    }

    ssl_certificate     /etc/ssl/certs/socket.plancal.local.crt;
    ssl_certificate_key /etc/ssl/certs/socket.plancal.local.key;
}
