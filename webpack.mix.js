const mix = require('laravel-mix');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
require('laravel-mix-polyfill');
const ESLintPlugin = require('eslint-webpack-plugin');
const webpack = require('webpack');

mix.babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import'],
});

if (mix.inProduction()) {
    mix.webpackConfig({
        cache: {
            type: 'filesystem'
        },
        output: {
            chunkFilename: 'js/[name].[contenthash].js'
        },
        plugins: [
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
            }),
            new MomentLocalesPlugin({
                localesToKeep: ['en', 'sv'],
            })
        ],
        // -- Chunk optimizations that can be applied if needed.
        // -- This makes the build take a while longer and should not be used if not production
        //
        // optimization: {
        //     splitChunks: {
        //         cacheGroups: {
        //             common: {
        //                 minChunks: 2,
        //                 chunks: 'all',
        //                 priority: 10,
        //                 reuseExistingChunk: true,
        //                 enforce: true
        //             }
        //         }
        //     }
        // }
    });
} else {
    mix.webpackConfig({
        output: {
            chunkFilename: 'js/[name].[contenthash].js'
        },
        plugins: [
            new webpack.DefinePlugin({
                __VUE_OPTIONS_API__: true,
                __VUE_PROD_DEVTOOLS__: false,
            }),
            new ESLintPlugin({
                extensions: ['js', 'vue'],
                fix: false,
                emitError: true,
                emitWarning: true,
                failOnError: true
            })
        ]
    });
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const compileEnvs = (function () {
    const allEnvs = ['web'];
    let filterEnvs = allEnvs;
    if (process.env.npm_config_onlyenv) {
        filterEnvs = process.env.npm_config_onlyenv.split(',');
    }
    return allEnvs.filter(compileEnv => filterEnvs.includes(compileEnv));
})();

if (compileEnvs.includes('web')) {
    console.info('compiling WEB');
    mix.js('resources/web/js/app.js', 'public/web/js').vue({ version: 3, runtimeOnly: true })
        .sass('resources/web/sass/app.scss', 'public/web/css')
        .options({ processCssUrls: false })
        .polyfill({
            enabled: true,
            useBuiltIns: 'usage'
        });
}

mix.copyDirectory('resources/assets', 'public/assets');

if (mix.inProduction()) {
    mix.version();
} else {
    const availableBsyncUrls = ['game','web','admin'];
    if (
        typeof process.env.npm_config_bsync !== 'undefined' &&
        availableBsyncUrls.includes(process.env.npm_config_bsync)
    ) {
        let proxyUrl = process.env.MIX_BROWSER_SYNC_URL;
        switch (process.env.npm_config_bsync) {
            case 'web':
                proxyUrl = process.env.MIX_WEB_URL;
                break;
        }

        mix.browserSync({
            proxy: proxyUrl,
            https: {
                key: process.env.MIX_BROWSER_SYNC_SSL_KEY,
                cert: process.env.MIX_BROWSER_SYNC_SSL_CERT
            },
            cors: true,
            open: false,
            files: [
                'public/*'
            ]
        });
    }
}
