<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Club
 *
 * @property int $id
 * @property string $name
 * @property mixed|null $description
 * @property string|null $short_description
 * @property int $member_fee
 * @property bool $accepting_new_members
 * @property bool $accept_without_approval
 * @property int|null $created_by_id
 * @property \Illuminate\Support\Carbon|null $disabled_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $acceptedUsers
 * @property-read int|null $accepted_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $admins
 * @property-read int|null $admins_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $adminsAndModerators
 * @property-read int|null $admins_and_moderators_count
 * @property-read \App\Models\User|null $createdBy
 * @property-read mixed $badge
 * @property-read int $cached_club_users_count
 * @property-read mixed $cover_image
 * @property-read string|null $description_html
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $pendingUsers
 * @property-read int|null $pending_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|Club newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Club query()
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereAcceptWithoutApproval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereAcceptingNewMembers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereActive()
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereCreatedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereDisabledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereMemberFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereShortDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Club whereUpdatedAt($value)
 */
	class IdeHelperClub {}
}

namespace App\Models{
/**
 * App\Models\ClubUser
 *
 * @property int $club_id
 * @property int $user_id
 * @property int $role
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Club $club
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ClubUser whereUserId($value)
 */
	class IdeHelperClubUser {}
}

namespace App\Models{
/**
 * App\Models\Comment
 *
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property string $body
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Post $post
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comment whereUserId($value)
 */
	class IdeHelperComment {}
}

namespace App\Models{
/**
 * App\Models\Course
 *
 * @property int $id
 * @property string $in_game_id
 * @property int|null $map_pack_id
 * @property string $title
 * @property string|null $description
 * @property array $tees
 * @property int $hcp_rebasing
 * @property bool|null $is_default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Hole[] $holes
 * @property-read int|null $holes_count
 * @property-read \App\Models\MapPack|null $mapPack
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $rounds
 * @property-read int|null $rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Query\Builder|Course onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereHcpRebasing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereInGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereMapPackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTees($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Course withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Course withoutTrashed()
 */
	class IdeHelperCourse {}
}

namespace App\Models{
/**
 * App\Models\CourseUser
 *
 * @property int $id
 * @property int $course_id
 * @property int $user_id
 * @property int $map_pack_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Course $course
 * @property-read \App\Models\MapPack $mapPack
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereMapPackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CourseUser whereUserId($value)
 */
	class IdeHelperCourseUser {}
}

namespace App\Models{
/**
 * App\Models\Device
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Device newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Device query()
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Device whereUserId($value)
 */
	class IdeHelperDevice {}
}

namespace App\Models{
/**
 * App\Models\GameIdentifier
 *
 * @property string $uuid
 * @property int $user_id
 * @property string|null $hardware_key
 * @property string|null $hardware_unit
 * @property string|null $software_version
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier query()
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereHardwareKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereHardwareUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereSoftwareVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameIdentifier whereUuid($value)
 */
	class IdeHelperGameIdentifier {}
}

namespace App\Models{
/**
 * App\Models\GameVersion
 *
 * @property int $id
 * @property string $uuid
 * @property string $version
 * @property int $latest
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $download_link
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection $files
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion query()
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereLatest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|GameVersion whereVersion($value)
 */
	class IdeHelperGameVersion {}
}

namespace App\Models{
/**
 * App\Models\Hole
 *
 * @property int $id
 * @property int $course_id
 * @property int $number
 * @property int $par
 * @property int $hcp
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Course $course
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $rounds
 * @property-read int|null $rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stroke[] $strokes
 * @property-read int|null $strokes_count
 * @method static \Illuminate\Database\Eloquent\Builder|Hole newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Hole newQuery()
 * @method static \Illuminate\Database\Query\Builder|Hole onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Hole query()
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereHcp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole wherePar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Hole whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Hole withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Hole withoutTrashed()
 */
	class IdeHelperHole {}
}

namespace App\Models{
/**
 * App\Models\MapPack
 *
 * @property int $id
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \App\Models\MapPackVersion|null $latestVersion
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MapPackVersion[] $versions
 * @property-read int|null $versions_count
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack query()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPack whereUpdatedAt($value)
 */
	class IdeHelperMapPack {}
}

namespace App\Models{
/**
 * App\Models\MapPackVersion
 *
 * @property int $id
 * @property int $map_pack_id
 * @property string $uuid
 * @property string $version
 * @property int $latest
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read string $download_link
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection $files
 * @property-read \App\Models\MapPack $mapPack
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion query()
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereLatest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereMapPackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MapPackVersion whereVersion($value)
 */
	class IdeHelperMapPackVersion {}
}

namespace App\Models{
/**
 * App\Models\Order
 *
 * @property string $id
 * @property string|null $stripe_id
 * @property int|null $user_id
 * @property int $status
 * @property int $price_type
 * @property int $amount_subtotal
 * @property int $amount_total
 * @property string|null $currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderLine[] $lines
 * @property-read int|null $lines_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAmountSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAmountTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order wherePriceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 */
	class IdeHelperOrder {}
}

namespace App\Models{
/**
 * App\Models\OrderLine
 *
 * @property string $id
 * @property string $order_id
 * @property string|null $stripe_id
 * @property string|null $stripe_price_id
 * @property string|null $stripe_product_id
 * @property int|null $product_id
 * @property int|null $product_price_id
 * @property int $amount_subtotal
 * @property int $amount_total
 * @property string|null $currency
 * @property int $quantity
 * @property int $product_type
 * @property array|null $meta
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereAmountSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereAmountTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereMeta($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereStripePriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereStripeProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderLine whereUpdatedAt($value)
 */
	class IdeHelperOrderLine {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property int $id
 * @property int $created_by_id
 * @property int|null $club_id
 * @property string|null $title
 * @property string|null $body
 * @property mixed|null $settings
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Club|null $club
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Models\User $createdBy
 * @property-read int $cached_comments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Post query()
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereClubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereCreatedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereUpdatedAt($value)
 */
	class IdeHelperPost {}
}

namespace App\Models{
/**
 * App\Models\PostUser
 *
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property int $action
 * @property int|null $action_sub_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Post $post
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereActionSubType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PostUser whereUserId($value)
 */
	class IdeHelperPostUser {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property string|null $stripe_id
 * @property int $sync_to_stripe
 * @property int $type
 * @property int|null $category
 * @property int $unlisted
 * @property int|null $max_qty
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $images
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductMeta[] $meta
 * @property-read int|null $meta_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductPrice[] $prices
 * @property-read int|null $prices_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereMaxQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSyncToStripe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUnlisted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Product withoutTrashed()
 */
	class IdeHelperProduct {}
}

namespace App\Models{
/**
 * App\Models\ProductMeta
 *
 * @property int $id
 * @property int $product_id
 * @property string $meta_key
 * @property string|null $meta_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereMetaKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereMetaValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductMeta whereUpdatedAt($value)
 */
	class IdeHelperProductMeta {}
}

namespace App\Models{
/**
 * App\Models\ProductPrice
 *
 * @property int $id
 * @property int $product_id
 * @property string|null $stripe_id
 * @property int $type
 * @property int $price
 * @property string|null $currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read string $formatted_price
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice newQuery()
 * @method static \Illuminate\Database\Query\Builder|ProductPrice onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductPrice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ProductPrice withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ProductPrice withoutTrashed()
 */
	class IdeHelperProductPrice {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $title
 * @property int|null $level
 * @property int|null $scope
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @property-read int|null $abilities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereAssignedTo($model, ?array $keys = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCan($ability, $scope = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereScope($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 */
	class IdeHelperRole {}
}

namespace App\Models{
/**
 * App\Models\Round
 *
 * @property int $id
 * @property string|null $title
 * @property int $visibility
 * @property int $status
 * @property int|null $created_by_id
 * @property int $course_id
 * @property int|null $tournament_id
 * @property int|null $tournament_cut
 * @property int $game_mode
 * @property int $max_players
 * @property int $pin_seed
 * @property array $settings
 * @property \Illuminate\Support\Carbon|null $start_at
 * @property \Illuminate\Support\Carbon|null $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $acceptedUsers
 * @property-read int|null $accepted_users_count
 * @property-read \App\Models\Course $course
 * @property-read \App\Models\User|null $createdBy
 * @property-read \App\Models\Scorecard|null $currentLeaderAsc
 * @property-read \App\Models\Scorecard|null $currentLeaderDesc
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $finishedUsers
 * @property-read int|null $finished_users_count
 * @property-read bool|null $is_handicap_eligible
 * @property-read bool|null $is_using_handicap
 * @property-read array|null $lobby_route
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Hole[] $holes
 * @property-read int|null $holes_count
 * @property-read \App\Models\RoundUser|null $myRoundRelation
 * @property-read \App\Models\Scorecard|null $myScorecard
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $pendingUsers
 * @property-read int|null $pending_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Scorecard[] $scorecards
 * @property-read int|null $scorecards_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Stroke[] $strokes
 * @property-read int|null $strokes_count
 * @property-read \App\Models\Tournament|null $tournament
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $unfinishedUsers
 * @property-read int|null $unfinished_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Hole[] $unplayedHoles
 * @property-read int|null $unplayed_holes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \App\Models\Wager|null $wager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|Round activeWithUser(int $userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Round newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Round newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Round query()
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereCreatedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereGameMode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereMaxPlayers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round wherePinSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereTournamentCut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereTournamentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereUserCanStart(int $userId)
 * @method static \Illuminate\Database\Eloquent\Builder|Round whereVisibility($value)
 */
	class IdeHelperRound {}
}

namespace App\Models{
/**
 * App\Models\RoundUser
 *
 * @property int $round_id
 * @property int $user_id
 * @property int $status
 * @property int $play_status
 * @property int|null $connected_to_id
 * @property int|null $connected_to_status
 * @property array|null $settings
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $connectedTo
 * @property-read \App\Models\Round $round
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereConnectedToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereConnectedToStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser wherePlayStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereRoundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoundUser whereUserId($value)
 */
	class IdeHelperRoundUser {}
}

namespace App\Models{
/**
 * App\Models\Score
 *
 * @property int $id
 * @property int $scorecard_id
 * @property int $hole_id
 * @property int|null $score
 * @property int|null $score_hcp
 * @property int|null $strokes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Hole $hole
 * @property-read \App\Models\Scorecard $scorecard
 * @method static \Illuminate\Database\Eloquent\Builder|Score newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Score newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Score query()
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereHoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereScoreHcp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereScorecardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereStrokes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Score whereUpdatedAt($value)
 */
	class IdeHelperScore {}
}

namespace App\Models{
/**
 * App\Models\Scorecard
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $round_id
 * @property int|null $tournament_id
 * @property int $course_id
 * @property int|null $total_score
 * @property string|null $hcp
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Course $course
 * @property-read int|null $current_position
 * @property-read \App\Models\Score|null $latestScore
 * @property-read \App\Models\Round $round
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Score[] $scores
 * @property-read int|null $scores_count
 * @property-read \App\Models\Tournament|null $tournament
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard query()
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereHcp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereRoundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereTotalScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereTournamentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Scorecard withCurrentPosition(int $gameMode)
 */
	class IdeHelperScorecard {}
}

namespace App\Models{
/**
 * App\Models\Stroke
 *
 * @property int $id
 * @property int $user_id
 * @property int $round_id
 * @property int $hole_id
 * @property array $preconditions
 * @property array $stroke_data
 * @property array $stroke_result
 * @property array|null $ball_path
 * @property array|null $surface_hits
 * @property array|null $calculated_values
 * @property int $mulligan
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Hole $hole
 * @property-read \App\Models\Round $round
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke query()
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereBallPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereCalculatedValues($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereHoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereMulligan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke wherePreconditions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereRoundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereStrokeData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereStrokeResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereSurfaceHits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Stroke whereUserId($value)
 */
	class IdeHelperStroke {}
}

namespace App\Models{
/**
 * App\Models\Tournament
 *
 * @property int $id
 * @property int|null $tour_id
 * @property string $title
 * @property string|null $subtitle
 * @property string|null $start_at
 * @property string|null $end_at
 * @property array $description
 * @property int|null $max_players
 * @property array|null $settings
 * @property int $winning_logic
 * @property int|null $hcp_limit
 * @property int|null $prize_sum
 * @property int|null $prize_token_type
 * @property int|null $entry_fee
 * @property int|null $entry_fee_token_type
 * @property \Illuminate\Support\Carbon|null $published_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $acceptedUsers
 * @property-read int|null $accepted_users_count
 * @property-read mixed $cover_image
 * @property-read string|null $description_html
 * @property-read mixed $image
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\TournamentUser|null $myTournamentRelation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $pendingUsers
 * @property-read int|null $pending_users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $rounds
 * @property-read int|null $rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereActive()
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereEntryFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereEntryFeeTokenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereHcpLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereJoinable()
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereMaxPlayers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament wherePrizeSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament wherePrizeTokenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereTourId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tournament whereWinningLogic($value)
 */
	class IdeHelperTournament {}
}

namespace App\Models{
/**
 * App\Models\TournamentScorecard
 *
 * @property-read int|null $current_position
 * @property-read bool|null $is_tied
 * @property int $id
 * @property int $tournament_id
 * @property int|null $user_id
 * @property int $round_id
 * @property int|null $hole_id
 * @property int|null $score
 * @property int|null $strokes
 * @property int $acc_score
 * @property int|null $acc_strokes
 * @property string|null $hcp
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Hole|null $hole
 * @property-read \App\Models\Round $round
 * @property-read \App\Models\Tournament $tournament
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard query()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereAccScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereAccStrokes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereHcp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereHoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereRoundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereStrokes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereTournamentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard withCurrentPosition()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentScorecard withIsTied()
 */
	class IdeHelperTournamentScorecard {}
}

namespace App\Models{
/**
 * App\Models\TournamentUser
 *
 * @property int $tournament_id
 * @property int $user_id
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Tournament $tournament
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser whereTournamentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentUser whereUserId($value)
 */
	class IdeHelperTournamentUser {}
}

namespace App\Models{
/**
 * App\Models\Transaction
 *
 * @property string $id
 * @property string $wallet_id
 * @property int|null $user_id
 * @property string $transaction_uuid
 * @property int $value
 * @property int $type
 * @property int $action_type
 * @property string|null $ip
 * @property string|null $user_agent
 * @property string|null $related_type
 * @property string|null $related_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $initiatedBy
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $related
 * @property-read \App\Models\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereActionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereRelatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereRelatedType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereTransactionUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUserAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Transaction whereWalletId($value)
 */
	class IdeHelperTransaction {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $uuid
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string|null $display_name
 * @property string $locale
 * @property string|null $country_code
 * @property string|null $hcp
 * @property array|null $interface_settings
 * @property array|null $default_player_settings
 * @property array|null $default_game_settings
 * @property \Illuminate\Support\Carbon|null $disabled_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @property-read int|null $abilities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Club[] $acceptedClubs
 * @property-read int|null $accepted_clubs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $acceptedRounds
 * @property-read int|null $accepted_rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $allFriends
 * @property-read int|null $all_friends_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Club[] $clubs
 * @property-read int|null $clubs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Course[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Device[] $devices
 * @property-read int|null $devices_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $friends
 * @property-read int|null $friends_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GameIdentifier[] $gameIdentifiers
 * @property-read int|null $game_identifiers_count
 * @property-read mixed $avatar
 * @property-read int|null $friend_status
 * @property-read string $full_name
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Club[] $pendingClubs
 * @property-read int|null $pending_clubs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $pendingFriends
 * @property-read int|null $pending_friends_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $pendingRounds
 * @property-read int|null $pending_rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $playedHcpRounds
 * @property-read int|null $played_hcp_rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Round[] $rounds
 * @property-read int|null $rounds_count
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $sentFriendRequests
 * @property-read int|null $sent_friend_requests_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tournament[] $tournaments
 * @property-read int|null $tournaments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Wallet[] $wallets
 * @property-read int|null $wallets_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereActive()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCan($ability, $scope = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDefaultGameSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDefaultPlayerSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDisabledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereHcp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereInterfaceSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIs($role)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAll($role)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsNot($role)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUuid($value)
 */
	class IdeHelperUser {}
}

namespace App\Models{
/**
 * App\Models\Wager
 *
 * @property int $id
 * @property string $wallet_id
 * @property int $round_id
 * @property int $token_type
 * @property int $cost
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Round $round
 * @property-read \App\Models\Wallet $wallet
 * @method static \Illuminate\Database\Eloquent\Builder|Wager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wager newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wager query()
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereRoundId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereTokenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wager whereWalletId($value)
 */
	class IdeHelperWager {}
}

namespace App\Models{
/**
 * App\Models\Wallet
 *
 * @property string $id
 * @property string $owner_type
 * @property int $owner_id
 * @property int $balance
 * @property int $token_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereOwnerType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereTokenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Wallet whereUpdatedAt($value)
 */
	class IdeHelperWallet {}
}

