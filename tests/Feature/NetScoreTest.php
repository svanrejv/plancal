<?php

namespace Tests\Feature;

use App\Helpers\Handicap\HandicapScore;
use Tests\TestCase;

class NetScoreTest extends TestCase
{

    /**
     * @throws \Exception
     */
    public function testHandicapRounding() {
        $NetScore = new HandicapScore(40.4, $this->getTestCourseHoles());
        $this->assertEquals(40, $NetScore->getCalculatedHandicap());

        $NetScore = new HandicapScore(40.5, $this->getTestCourseHoles());
        $this->assertEquals(41, $NetScore->getCalculatedHandicap());

        $NetScore = new HandicapScore(-0.4, $this->getTestCourseHoles());
        $this->assertEquals(0, $NetScore->getCalculatedHandicap());
    }

    /**
     * @throws \Exception
     */
    public function testHoleExtraStroke() {
        $NetScore = new HandicapScore(18, $this->getTestCourseHoles());
        $this->assertEquals(1, $NetScore->getHoleExtraStrokes(1));

        $NetScore = new HandicapScore(36, $this->getTestCourseHoles());
        $this->assertEquals(2, $NetScore->getHoleExtraStrokes(1));

        $NetScore = new HandicapScore(23.2, $this->getTestCourseHoles());
        $this->assertEquals(2, $NetScore->getHoleExtraStrokes(18));
        $this->assertEquals(1, $NetScore->getHoleExtraStrokes(13));

        $NetScore = new HandicapScore(10, $this->getTestCourseHoles());
        $this->assertEquals(1, $NetScore->getHoleExtraStrokes(18));
        $this->assertEquals(0, $NetScore->getHoleExtraStrokes(1));
    }

    public function testMinusHcp() {
        $NetScore = new HandicapScore(-1, $this->getTestCourseHoles());
        $this->assertEquals(-1, $NetScore->getHoleExtraStrokes(1));

        $NetScore = new HandicapScore(-1, $this->getTestCourseHoles());
        $this->assertEquals(0, $NetScore->getHoleExtraStrokes(2));
    }

    public function testNetScore()
    {
        $testHoles = $this->getTestCourseHoles();
        foreach($testHoles as $testHole) {
            if($testHole->id === 1) {
                $this->assertEquals(3, $testHole->par);
            }
        }

        $NetScore = new HandicapScore(0, $testHoles);
        $this->assertEquals(3, $NetScore->getHoleNetScore(3, 1));

        $NetScore = new HandicapScore(36, $testHoles);
        $this->assertEquals(2, $NetScore->getHoleNetScore(4, 1));

        $NetScore = new HandicapScore(18, $testHoles);
        $this->assertEquals(3, $NetScore->getHoleNetScore(4, 1));
    }

    public function testMatchHandicap() {
        $Handicaps = HandicapScore::getMatchHandicapCalculator(10, 20, $this->getTestCourseHoles());
        $this->assertEquals(0, $Handicaps['Player1']->getCalculatedHandicap());
        $this->assertEquals(10, $Handicaps['Player2']->getCalculatedHandicap());

        $Handicaps = HandicapScore::getMatchHandicapCalculator(30, 15, $this->getTestCourseHoles());
        $this->assertEquals(15, $Handicaps['Player1']->getCalculatedHandicap());
        $this->assertEquals(0, $Handicaps['Player2']->getCalculatedHandicap());

        $Handicaps = HandicapScore::getMatchHandicapCalculator(10, 10, $this->getTestCourseHoles());
        $this->assertEquals(0, $Handicaps['Player1']->getCalculatedHandicap());
        $this->assertEquals(0, $Handicaps['Player2']->getCalculatedHandicap());

        $Handicaps = HandicapScore::getMatchHandicapCalculator(-2, 10, $this->getTestCourseHoles());
        $this->assertEquals(0, $Handicaps['Player1']->getCalculatedHandicap());
        $this->assertEquals(12, $Handicaps['Player2']->getCalculatedHandicap());

        $Handicaps = HandicapScore::getMatchHandicapCalculator(-3.4, 10.4, $this->getTestCourseHoles());
        $this->assertEquals(0, $Handicaps['Player1']->getCalculatedHandicap());
        $this->assertEquals(14, $Handicaps['Player2']->getCalculatedHandicap());

    }

    private function getTestCourseHoles() {
        // TODO: Improve this
        $holes = [];

        $indexes = [18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1];
        $pars = [3,3,4,4,5,3,5,4,4,5,5,4,3,4,5,4,4,3];

        for($i=1; $i<=18; $i++) {
            $hole = new \stdClass();
            $hole->id = $i;
            $hole->hcp = $indexes[($i-1)];
            $hole->par = $pars[($i-1)];
            $holes[] = $hole;
        }

        return $holes;
    }
}
