<?php

namespace Tests\Feature;

use App\Helpers\Handicap\Handicap;
use App\Helpers\Handicap\HandicapScore;
use App\Models\Hole;
use JetBrains\PhpStorm\ArrayShape;
use Tests\TestCase;

class HandicapTest extends TestCase {

    /**
     * @throws \Exception
     */
    public function testAdjustedGrossScore() {
        $playerHcp = 0;
        $courseId = 1;
        $tee = 'white';

        $Handicap = new Handicap($playerHcp, []);
        $slopeRating = $Handicap->getSlopeRating($courseId, $tee, 'full');
        $this->assertEquals(134, $slopeRating);

        $testRound = $this->generateTestRound('onPar');
        $HandicapScore = new HandicapScore($playerHcp, $testRound['holes']);
        $testHole = $testRound['holes'][0];

        $this->assertEquals(4, $Handicap->convertStrokesToAdjustedGrossScore(4,$testHole['par'] + $HandicapScore->getHoleExtraStrokes($testHole['id'])));
        $this->assertEquals($testHole['par']+2, $Handicap->convertStrokesToAdjustedGrossScore(15,$testHole['par'] + $HandicapScore->getHoleExtraStrokes($testHole['id'])));

        // Change handicap
        $HandicapScore = new HandicapScore(36, $testRound['holes']);
        $this->assertEquals(4, $Handicap->convertStrokesToAdjustedGrossScore(4,$testHole['par'] + $HandicapScore->getHoleExtraStrokes($testHole['id'])));
        $hcpPar = $testHole['par'] + $HandicapScore->getHoleExtraStrokes($testHole['id']);
        $this->assertEquals(
            $hcpPar + 2,
            $Handicap->convertStrokesToAdjustedGrossScore(15, $hcpPar)
        );
    }

    public function testNumberOfExtraStrokes() {
        $NetScore = new HandicapScore(27, $this->getTestCourseHoles());
        $extraStrokes = $NetScore->getNumberOfExtraStrokes();
        $this->assertEquals(2, $extraStrokes['18']);
        $this->assertEquals(1, $extraStrokes['1']);
    }

    public function testHandicapDefaultFallbackCalculation() {
        $currentHandicap = null;
        $Handicap = Handicap::calculateHandicap($currentHandicap, []);
        $this->assertEquals(40.4, $Handicap->newHandicap);
    }

    public function testHandicapBetteringCalculation() {
        $currentHandicap = 40.4;
        $lastRounds = $this->getTestRounds(1);
        $Handicap = Handicap::calculateHandicap($currentHandicap, $lastRounds);
        $this->assertEquals(-1.5, $Handicap->newHandicap);
    }

    public function testHandicapBetteringOnMultiRounds() {
        $currentHandicap = 40.4;
        $lastRounds = $this->getTestRounds(7);
        $Handicap = Handicap::calculateHandicap($currentHandicap, $lastRounds);

        $this->assertEquals(-1.5, $Handicap->newHandicap);
    }

    public function testPlayBadNoRaise() {
        $currentHandicap = 40.4;
        $lastRounds = $this->getTestRounds(1, 'bad');
        $Handicap = Handicap::calculateHandicap($currentHandicap, $lastRounds);

        $this->assertEquals(40.4, $Handicap->newHandicap);
    }

    public function testPlay9Holes() {
        $currentHandicap = 40.4;
        $lastRounds = $this->getTestRounds(1, 'onPar', 'front9');
        $Handicap = Handicap::calculateHandicap($currentHandicap, $lastRounds);

        $this->assertEquals(0.5, $Handicap->newHandicap);

        $lastRounds = $this->getTestRounds(1, 'onPar', 'back9');
        $Handicap = Handicap::calculateHandicap($currentHandicap, $lastRounds);

        $this->assertEquals(-2.0, $Handicap->newHandicap);
    }

    private function getTestRounds($num = 1, $resultType = 'onPar', $holesPlayed = 'full'): array
    {
        $rounds = [];
        for($i=0;$i<$num;$i++) {
            $rounds[] = $this->generateTestRound($resultType, $holesPlayed);
        }
        return $rounds;
    }

    private function getTestCourseHoles() {
        // TODO: Improve this
        $holes = [];

        $indexes = [18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1];
        $pars = [3,3,4,4,5,3,5,4,4,5,5,4,3,4,5,4,4,3];

        for($i=1; $i<=18; $i++) {
            $hole = new \stdClass();
            $hole->id = $i;
            $hole->hcp = $indexes[($i-1)];
            $hole->par = $pars[($i-1)];
            $holes[] = $hole;
        }

        return $holes;
    }

    #[ArrayShape([
        'handicap' => "float",
        'course_id' => "int",
        'tee' => "string",
        'holes' => "array",
        'holesPlayed' => "mixed|string",
        'courseHoles' => 'array'
    ])] private function generateTestRound($resultType, $holesPlayed = 'full'): array
    {
        $courseId = 1;
        $testRoundHoles = [];
        $holes = Hole::where('course_id', '=', $courseId)->get()->toArray();
        $courseHoles = $holes;

        if($holesPlayed === 'front9') {
            array_splice($holes, 0, 9);
        } else if($holesPlayed === 'back9') {
            array_splice($holes, 9, 9);
        }

        foreach($holes as $hole) {
            $strokes = $hole['par'];

            if($resultType === 'bad') {
                $strokes = $hole['par']*2;
            }

            $testRoundHoles[] = [
                'id' => $hole['id'],
                'par' => $hole['par'],
                'hcp' => $hole['hcp'],
                'strokes' => $strokes
            ];
        }

        return [
            'handicap' => 40.4,
            'course_id' => $courseId,
            'tee' => 'white',
            'holes' => $testRoundHoles,
            'holesPlayed' => $holesPlayed,
            'courseHoles' => $courseHoles
        ];
    }

}
